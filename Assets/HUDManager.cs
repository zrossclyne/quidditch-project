﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDManager : MonoBehaviour
{
    public MatchHandler matchHandler;
    public Player player;
    public Text topText;
    public Text bottomText;

    // Use this for initialization
    void Awake()
    {
        player = GetComponentInParent<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        if (matchHandler != null)
        {
            bottomText.text = PlayerTeam.Team_1.ToString().Replace("_", " ") + " " + matchHandler.m_score.teams[PlayerTeam.Team_1] + " - " + matchHandler.m_score.teams[PlayerTeam.Team_2] + " " + PlayerTeam.Team_2.ToString().Replace("_", " ");
        }

        if (player != null)
        {
            topText.text = player.m_playerTeam.ToString().Replace("_", " ") + " - " + player.m_playerType + (player.m_playerPosition == 0 ? "" : player.m_playerPosition.ToString());
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class RestrictGrab : MonoBehaviour
{
    public List<PlayerType> m_playerTypes = new List<PlayerType>();

    public VRTK_InteractableObject m_object;

    public Rigidbody m_rigidBody;

    private Vector3 m_beforeGrabVelocity;

    // Use this for initialization
    void Start()
    {
        if (m_object != null)
        {
            m_object.InteractableObjectGrabbed += ObjectGrabbed;
        }
    }

    private void ObjectGrabbed(object sender, InteractableObjectEventArgs e)
    {
        Player player = e.interactingObject.GetComponentInParent<Player>();

        if (player != null)
        {
            if (!m_playerTypes.Contains(player.m_playerType))
            {
                e.interactingObject.GetComponent<VRTK_InteractGrab>().ForceRelease(false);

                m_rigidBody.velocity = m_beforeGrabVelocity;
            }
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!m_object.IsGrabbed())
        {
            m_beforeGrabVelocity = m_rigidBody.velocity;
        }
    }
}

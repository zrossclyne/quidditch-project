﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zac;

public enum SnitchStates
{
	Idle,
	IdleConsider,
	Wander,
	WanderContinue,
	Flee,
	Dash
}

public class SnitchStateMachine : StateMachine
{
	#region Serialized Fields
	[SerializeField] private string m_currentState;

	// Generic
	[SerializeField] private Transform m_pitchOrigin;

	// Idle
	[SerializeField] private int m_idleMinDuration;
	[SerializeField] private int m_idleMaxDuration;

	// Idle Consider
	[Range(0, 1), SerializeField] private float m_idleExitChance;

	// Wander
	[SerializeField] private float m_wanderSpeed;
	[SerializeField] private float m_wanderVariance;
	[Range(0, 1), SerializeField] private float m_wanderContinueChance;

	// Flee
	[SerializeField] private float m_fleeSpeed;
	[SerializeField] private float m_fleeVariance;
	[SerializeField] private float m_fleeExitTime;
	[Range(0.3f, Mathf.PI * 2), SerializeField]	private float m_fleeTurnItensity;
	[SerializeField] private float m_fleeDashDurationMinimum;
	[SerializeField] private float m_fleeDashDurationMaxmimum;

	// Dash
	[SerializeField] private float m_dashForce;

	// Temp
	[SerializeField] private float m_scaleX;
	[SerializeField] private float m_scaleY;
	[SerializeField] private float m_scaleZ;
	#endregion

	#region Non-Serialized Fields
	// Generic
	[SerializeField, HideInInspector] private Rigidbody m_rigidbody;

	// Wander
	[SerializeField, HideInInspector] private Vector3 m_wanderDestination = Vector3.zero;

	// Flee Legacy
	[SerializeField, HideInInspector] private Vector3 m_fleeTurnVector;
	[SerializeField, HideInInspector] private bool m_fleeTriggered = false;

	// Flee
	[SerializeField, HideInInspector] private Vector3 m_fleeDestination = Vector3.zero;
	[SerializeField, HideInInspector] private List<GameObject> m_fleePlayers;
	[SerializeField, HideInInspector] private float m_fleeNextTurnIntensity = 0.3f;
	#endregion

	#region Private Properties
	private float WanderVariance
	{
		get
		{
			return Mathf.Pow(m_wanderVariance, 2);
		}
	}
	#endregion

	private delegate void SnitchEventHandler();

	#region Unity Events
	protected override void Awake()
	{
		base.Awake();
		m_rigidbody = GetComponent<Rigidbody>();

		m_fleePlayers = new List<GameObject>();

		// Register Callbacks
		OnStateEnter += OnStateChanged;
	}

	// Use this for initialization
	private void Start () 
	{
		Initialize(SnitchStates.Idle);
	}
	
	// Update is called once per frame
	protected override void Update () 
	{
		base.Update();

	}

	private void OnTriggerEnter(Collider other)
	{
		if(other.tag == "Player")
		{
			if(!m_fleePlayers.Contains(other.gameObject))
			{
				m_fleePlayers.Add(other.gameObject);
			}
		}
	}

	private void OnTriggerExit(Collider other)
	{
		if (other.tag == "Player")
		{
			if (m_fleePlayers.Contains(other.gameObject))
			{
				m_fleePlayers.Remove(other.gameObject);
			}
		}

		m_fleeTriggered = m_fleePlayers.Count > 0;

		if(!m_fleeTriggered)
		{
			StartCoroutine(FleeTimer());
		}
	}
	#endregion

	#region Public Functionality

	#endregion

	#region Private Functionality
	private Vector3 GetMovementDestination()
	{
		Vector3 value = Random.insideUnitSphere;
		return m_pitchOrigin.position + Vector3.Scale(value, new Vector3(m_scaleX, m_scaleY, m_scaleZ));
	}

	private bool RandomCondition(ref float chance)
	{
		return chance <= Random.Range(0.0f, 1.0f);
	}

	private Vector3 AverageVectors(List<GameObject> vectors)
	{
		float x = 0;
		float y = 0;
		float z = 0;

		for(int i = 0; i < vectors.Count; i++)
		{
			x += vectors[i].transform.position.x;
			y += vectors[i].transform.position.y;
			z += vectors[i].transform.position.z;
		}

		return new Vector3(x, y, z) / (float)(vectors.Count > 0 ? vectors.Count : 1);
	}

	private void Log(object message)
	{
#if UNITY_EDITOR
		Debug.Log(message);
#endif
	}
	#endregion

	#region Coroutines
	private IEnumerator ExecuteDurationYield(SnitchStates currentState, SnitchStates targetState, float duration)
	{
		float startTime = Time.fixedTime;

		while (Time.fixedTime - startTime < m_idleMinDuration && GetCurrentState<SnitchStates>() == currentState)
		{
			yield return new WaitForEndOfFrame();
		}

		if (GetCurrentState<SnitchStates>() == currentState)
		{
			Transition(targetState);

#if UNITY_EDITOR
			if (currentState == SnitchStates.IdleConsider)
			{
				Log("Time met. Forcing wander.");
			}
#endif
		}
	}

	private IEnumerator FleeTimer()
	{
		float startTime = Time.fixedTime;

		while(Time.fixedTime - startTime < m_fleeExitTime && !m_fleeTriggered)
		{
			yield return new WaitForEndOfFrame();
		}

		if(!m_fleeTriggered)
		{
			Transition(SnitchStates.Wander);
		}
	}

	private IEnumerator FleeToDash()
	{
		float duration = Random.Range(m_fleeDashDurationMinimum, m_fleeDashDurationMaxmimum);

		yield return new WaitForSeconds(duration);

		Transition(SnitchStates.Dash);
	}
	#endregion

	#region Event Callbacks
	private void OnStateChanged()
	{
		m_currentState = GetCurrentState<SnitchStates>().ToString();
	}
	#endregion

	#region StateMachineEvents
	void Idle_Enter()
	{
		StartCoroutine(ExecuteDurationYield(SnitchStates.Idle, SnitchStates.IdleConsider, m_idleMinDuration));
		Log("Idling");
	}

	void Idle_Update()
	{

	}

	void Idle_Exit()
	{

	}

	void IdleConsider_Enter()
	{
		StartCoroutine(ExecuteDurationYield(SnitchStates.IdleConsider, SnitchStates.Wander, m_idleMaxDuration - m_idleMinDuration));
		Log("Idling while considering wander.");
	}

	void IdleConsider_Update()
	{
		if(RandomCondition(ref m_idleExitChance))
		{
			Log("Wandering normally.");
			Transition(SnitchStates.Wander);
		}
	}

	void IdleConsider_Exit()
	{

	}

	void Wander_Enter()
	{
		m_wanderDestination = GetMovementDestination();
		Log("Wandering");
	}

	void Wander_Update()
	{
		// Perform Movement
		Vector3 offset = m_wanderDestination - transform.position;

		float distance = offset.magnitude;
		float distanceSqrd = distance * distance;

		m_rigidbody.velocity = offset.normalized * m_wanderSpeed;

		// Are we within the variance, or have we gone too far.
		if (distance <= WanderVariance || (m_wanderDestination - (transform.position + offset.normalized)).sqrMagnitude > distanceSqrd)
		{
			m_rigidbody.velocity = Vector3.zero;

			SnitchStates nextState = RandomCondition(ref m_wanderContinueChance) ? SnitchStates.WanderContinue : SnitchStates.Idle;
			Transition(nextState);
		}
	}

	void Wander_Exit()
	{

	}

	void WanderContinue_Enter()
	{
		Log("Continuing wander.");
		Transition(SnitchStates.Wander);
	}

	void Flee_Enter()
	{
        Log("Fleeing");
		// Set initial velocity here
		Vector3 velocity = AverageVectors(m_fleePlayers);

	    if (velocity.sqrMagnitude > 0)
	    {
	        m_rigidbody.velocity = velocity;
	    }

		m_fleeDestination = GetMovementDestination();

		StartCoroutine(FleeToDash());
	}

	void Flee_Update()
	{
		if((transform.position - m_fleeDestination).magnitude < m_fleeVariance)
		{
			m_fleeDestination = GetMovementDestination();
			m_fleeNextTurnIntensity = Random.Range(0.3f, m_fleeTurnItensity);
		}
		else
		{
			Vector3 snitchVelocity = m_fleeSpeed * Vector3.RotateTowards(m_rigidbody.velocity, m_fleeDestination - transform.position, Time.deltaTime * m_fleeNextTurnIntensity, 0.0f);

			snitchVelocity = snitchVelocity.normalized * m_fleeSpeed;
			m_rigidbody.velocity = snitchVelocity;

			Globals.CapVelocity(ref m_rigidbody, m_fleeSpeed);
		}
	}

	void Flee_Exit()
	{
		
	}

	void Dash_Enter()
	{
        Log("Dashing");

		Vector3 dashDirection = Vector3.right * (Random.Range(0, 2) == 0 ? 1.0f : -1.0f);

		Vector3 velocity = Vector3.Scale(m_rigidbody.velocity, new Vector3(1, 0, 1));

		float angle = Vector3.Angle(Vector3.forward, velocity);

		dashDirection = Quaternion.Euler(0, angle, 0) * dashDirection;

		m_rigidbody.velocity = dashDirection.normalized * m_dashForce;

		Transition(SnitchStates.Flee);
	}

	void Dash_Update()
	{

	}

	void Dash_Exit()
	{

	}

	#region Conditions
	bool Wander_Flee_Condition()
	{
		return m_fleePlayers.Count > 0;
	}

	bool WanderContinue_Flee_Condition()
	{
		return m_fleePlayers.Count > 0;
	}

	bool Idle_Flee_Condition()
	{
		return m_fleePlayers.Count > 0;
	}

	bool Flee_Wander_Condition()
	{
		//return false;
		return m_fleePlayers.Count == 0;
	}
	#endregion
	#endregion
	// To do automated transition create functions like this
	// FromState_ToState_Condition

    void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(m_pitchOrigin.position, new Vector3(m_scaleX, m_scaleY, m_scaleZ));
    }
}

﻿#define GIZMO_SHOW

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BludgerAI : MonoBehaviour
{
	public float m_pursueDistance;

	public Rigidbody m_bludgerRigidBody;

	public bool m_roaming;

	public bool m_colliding;

	private PlayerTeam m_lastPlayerTeam;
	
	public float m_lastTime;

	public float m_pursueSeconds;

	// Use this for initialization
	void Start ()
	{
		m_lastTime = Time.time - 100.0f;
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		if (Time.time - m_lastTime >= m_pursueSeconds)
		{
			m_lastTime = Time.time;

			Player[] players = GameObject.FindObjectsOfType<Player>();

			Player closestPlayer = null;
			float closestDistance = float.MaxValue;

			foreach (Player player in players)
			{
				float distance = Vector3.Distance(player.transform.position, transform.position);

				if (player.m_playerTeam != m_lastPlayerTeam)
				{
					if (distance < closestDistance)
					{
						closestDistance = distance;
						closestPlayer = player;
					}
				}
			}

			if (closestDistance <= m_pursueDistance)
			{
				// Should pursue
				EnablePursue(closestPlayer.GetComponent<Rigidbody>());
			}
			else
			{
				EnableWander();
			}
		}
		
		//Globals.ApplyDrag(ref m_bludgerRigidBody);
	}

	void EnablePursue(Rigidbody target)
	{
		GetComponent<UnitWander>().enabled = false;

		UnitPursue unitPursue = GetComponent<UnitPursue>();
		unitPursue.enabled = true;

		unitPursue.m_target = target;
	}

	void EnableWander()
	{
		GetComponent<UnitPursue>().m_target = null;
		GetComponent<UnitPursue>().enabled = false;

		GetComponent<UnitWander>().enabled = true;
	}

	void OnCollisionEnter(Collision collision)
	{
		Player player = collision.collider.gameObject.GetComponent<Player>();

		if (player != null)
		{
			Debug.Log("Collided with a player");

			m_lastPlayerTeam = player.m_playerTeam;

			EnableWander();
		}
		else
		{
			// Could be a bat
			if (collision.gameObject.name == "Bat" && !m_colliding)
			{
				m_bludgerRigidBody.velocity = Vector3.zero;

				m_colliding = true;
				
				float forceMultiplier = GetBatForce(collision.gameObject.GetComponent<BeatersBat>());

				Vector3 direction = (transform.position - collision.contacts[0].point).normalized;
				m_bludgerRigidBody.AddForce(direction * forceMultiplier, ForceMode.Impulse);
				
				m_lastPlayerTeam = PlayerTeam.NoTeam;
				EnableWander();
			}
		}
	}

	float GetBatForce(BeatersBat beatersBat)
	{
		return beatersBat.speed * 5.0f;
	}

	void OnCollisionExit(Collision collision)
	{
		m_colliding = false;
	}

#if UNITY_EDITOR && GIZMO_SHOW
	void OnDrawGizmos()
	{
		Gizmos.color = Color.white;

		foreach (Player p in GameObject.FindObjectsOfType<Player>())
		{
			Gizmos.DrawWireSphere(p.transform.position, m_pursueDistance);
		}
	}
#endif
}
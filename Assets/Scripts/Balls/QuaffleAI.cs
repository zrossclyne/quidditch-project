﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using VRTK;

public class QuaffleAI : MonoBehaviour
{
    public MatchHandler m_matchHandler;

	public Collider m_outerCollider;

	public Rigidbody m_quaffleRigidBody;

	public VRTK_InteractableObject m_object;
	public float m_scalar = 2.5f;

	public Player m_lastPlayer;

	// Use this for initialization
	void Start () 
	{
		m_quaffleRigidBody.AddForce(Vector3.up * 2.0f, ForceMode.Impulse);

		m_object.InteractableObjectGrabbed += InteractableObjectGrabbed;
		m_object.InteractableObjectUngrabbed += InteractableObjectUngrabbed;
	}

	private void InteractableObjectGrabbed(object sender, InteractableObjectEventArgs e)
	{
		m_outerCollider.enabled = false;
	}

	private void InteractableObjectUngrabbed(object sender, InteractableObjectEventArgs e)
	{
	    m_quaffleRigidBody.useGravity = true;

        m_outerCollider.enabled = true;

		Player player = e.interactingObject.GetComponentInParent<Player>();
		PlayerHand playerHand = e.interactingObject.GetComponentInParent<PlayerHand>();

		m_quaffleRigidBody.velocity = player.GetComponent<Rigidbody>().velocity;

		
		m_quaffleRigidBody.AddForce(playerHand.m_direction.normalized * playerHand.m_speed * m_scalar);
	}

	// Update is called once per frame
	void FixedUpdate () 
	{
		if (!m_object.IsGrabbed())
		{
			//Globals.ApplyDrag(ref m_quaffleRigidBody);
		}
	}
}

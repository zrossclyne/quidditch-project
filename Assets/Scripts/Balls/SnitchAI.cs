﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class SnitchAI : MonoBehaviour
{
    public VRTK_InteractableObject m_object;

    public Rigidbody m_rigidBody;

    public SnitchStateMachine m_stateMachine;

    public Collider m_collider;

	// Use this for initialization
	void Start () 
	{
	    if (m_object != null)
	    {
	        m_object.InteractableObjectGrabbed += InteractableObjectGrabbed;
	        m_object.InteractableObjectUngrabbed += InteractableObjectUngrabbed;
	    }
	}

    private void InteractableObjectGrabbed(object sender, InteractableObjectEventArgs e)
    {
        if (m_stateMachine != null)
        {
            m_stateMachine.enabled = false;
        }

        m_rigidBody.velocity = Vector3.zero;

        m_collider.enabled = false;
    }

    private void InteractableObjectUngrabbed(object sender, InteractableObjectEventArgs e)
    {
        if (m_stateMachine != null)
        {
            m_stateMachine.enabled = true;
        }

        m_collider.enabled = true;
    }

    // Update is called once per frame
    void Update () 
	{
		
	}
}

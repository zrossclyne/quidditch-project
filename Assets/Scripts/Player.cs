﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VRTK;
using Object = System.Object;

public enum PlayerTeam
{
    NoTeam,
    Team_1,
    Team_2
}

public enum PlayerType
{
    Keeper,
    Beater,
    Chaser,
    Seeker
}

public class Player : MonoBehaviour
{
    public PlayerTeam m_playerTeam;

    public List<VRTK_InteractableObject> m_objects;

    public VRTK_InteractGrab m_leftHand;
    public VRTK_InteractGrab m_rightHand;

    public PlayerType m_playerType;

    public int m_playerPosition;

    public bool m_outOfBounds;
    public Text m_outOfBoundsText;

	// Use this for initialization
	void Start ()
	{
	    string positionString = m_playerType.ToString();

	    if (m_playerType == PlayerType.Chaser || m_playerType == PlayerType.Beater)
	    {
	        positionString += " " + m_playerPosition;
	    }
        
        transform.position = GameObject.Find("[Starting Positions]").transform.Find("[" + m_playerTeam.ToString().Replace('_', ' ') + "]").transform.Find(positionString).position;

	    float startZ = -0.75f * (m_objects.Count / 2.0f);
	    float stepZ = 1.5f;
	    foreach (var obj in m_objects)
	    {
            Vector3 pos = new Vector3(transform.position.x, transform.position.y + 1.0f, transform.position.z + startZ);
	        obj.gameObject.transform.position = pos;

	        startZ += stepZ;
	    }
	}
    
    // Update is called once per frame
    void FixedUpdate ()
    {
        if (m_outOfBoundsText != null)
        {
            m_outOfBoundsText.enabled = m_outOfBounds;
        }
    }
}

﻿using UnityEngine;

public class UnitWander : MonoBehaviour
{
    public Vector3 m_origin;

    // Min and Max values for X and Z, and Max value for Y (from origin)
    public float m_maxX;
    public float m_maxY;
    public float m_maxZ;
    
    public float m_maxVelocity;

    public Rigidbody m_rigidBody;

    public float m_seekDistance;

    public GameObject m_seekObject;
    public float m_seekRadius;
    public float m_angleChange;

    public float m_wanderAngleX;
    public float m_wanderAngleY;
    public float m_wanderAngleZ;

    private static System.Random m_random;

    private void Start()
    {
        if (m_random == null)
        {
            m_random = new System.Random((int)Time.time * 1000);
        }

        m_seekObject.transform.position = m_origin;
    }
    
    private void FixedUpdate()
    {
        m_seekObject.transform.rotation = Quaternion.Euler(m_wanderAngleX, m_wanderAngleY, m_wanderAngleZ);
        
        Vector3 newSeekPosition = m_seekObject.transform.position + m_seekObject.transform.forward.normalized * m_seekRadius;

        if (newSeekPosition.x > m_origin.x + (m_maxX / 2.0f))
        {
            newSeekPosition.x = m_origin.x + (m_maxX / 2.0f);
        }
        else if (newSeekPosition.x < m_origin.x - (m_maxX / 2.0f))
        {
            newSeekPosition.x = m_origin.x - (m_maxX / 2.0f);
        }

        if (newSeekPosition.y > m_origin.y + (m_maxY / 2.0f))
        {
            newSeekPosition.y = m_origin.y + (m_maxY / 2.0f);
        }
        else if (newSeekPosition.y < m_origin.y - (m_maxY / 2.0f))
        {
            newSeekPosition.y = m_origin.y - (m_maxY / 2.0f);
        }

        if (newSeekPosition.z > m_origin.z + (m_maxZ / 2.0f))
        {
            newSeekPosition.z = m_origin.z + (m_maxZ / 2.0f);
        }
        else if (newSeekPosition.z < m_origin.z - (m_maxZ / 2.0f))
        {
            newSeekPosition.z = m_origin.z - (m_maxZ / 2.0f);
        }

        m_seekObject.transform.position = newSeekPosition;
        
        m_wanderAngleY += ((m_random.Next(0, 100) / 100.0f) * m_angleChange) - (m_angleChange * 0.5f);

        Vector3 direction = (m_seekObject.transform.position - transform.position).normalized * 5.0f;

        m_rigidBody.AddForce(direction);

        Vector3 howFar = Vector3.zero;
        Vector3 forceDirection = Vector3.zero;
        
        // Now handle the boundaries
        if (transform.position.x > m_origin.x + m_maxX)
        {
            // Too far right
            howFar.x = transform.position.x - (m_origin.x + m_maxX);
        }
        else if (transform.position.x < m_origin.x - m_maxX)
        {
            // Too far left
            howFar.x = transform.position.x - (m_origin.x - m_maxX);
        }

        if (transform.position.y > m_origin.y + m_maxY)
        {
            // Too high
            howFar.y = transform.position.y - (m_origin.y + m_maxY);
        }
        else if (transform.position.y < m_origin.y - m_maxY)
        {
            // Too low
            howFar.y = transform.position.y - (m_origin.y - m_maxY);
        }

        if (transform.position.z > m_origin.z + m_maxZ)
        {
            // Too far forward
            howFar.z = transform.position.z - (m_origin.z + m_maxZ);
        }
        else if (transform.position.z < m_origin.z - m_maxZ)
        {
            // Too far back
            howFar.z = transform.position.z - (m_origin.z - m_maxZ);
        }
        
        m_rigidBody.AddForce(howFar * -2.5f);
        
        // Cap velocity
        Globals.CapVelocity(ref m_rigidBody, m_maxVelocity);
    }

#if UNITY_EDITOR && GIZMO_SHOW
    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;

        Gizmos.DrawWireSphere(m_origin, 1.0f);

        Gizmos.color = Color.green;

        // Left Back 
        Gizmos.DrawWireSphere(new Vector3(m_origin.x - m_maxX, m_origin.y - m_maxY, m_origin.z - m_maxZ), 1.0f);
        Gizmos.DrawWireSphere(new Vector3(m_origin.x - m_maxX, m_origin.y, m_origin.z - m_maxZ), 1.0f);
        Gizmos.DrawWireSphere(new Vector3(m_origin.x - m_maxX, m_origin.y + m_maxY, m_origin.z - m_maxZ), 1.0f);

        // Right Back
        Gizmos.DrawWireSphere(new Vector3(m_origin.x + m_maxX, m_origin.y - m_maxY, m_origin.z - m_maxZ), 1.0f);
        Gizmos.DrawWireSphere(new Vector3(m_origin.x + m_maxX, m_origin.y, m_origin.z - m_maxZ), 1.0f);
        Gizmos.DrawWireSphere(new Vector3(m_origin.x + m_maxX, m_origin.y + m_maxY, m_origin.z - m_maxZ), 1.0f);

        // Left Middle
        Gizmos.DrawWireSphere(new Vector3(m_origin.x - m_maxX, m_origin.y - m_maxY, m_origin.z), 1.0f);
        Gizmos.DrawWireSphere(new Vector3(m_origin.x - m_maxX, m_origin.y, m_origin.z), 1.0f);
        Gizmos.DrawWireSphere(new Vector3(m_origin.x - m_maxX, m_origin.y + m_maxY, m_origin.z), 1.0f);

        // Right Middle
        Gizmos.DrawWireSphere(new Vector3(m_origin.x + m_maxX, m_origin.y - m_maxY, m_origin.z), 1.0f);
        Gizmos.DrawWireSphere(new Vector3(m_origin.x + m_maxX, m_origin.y, m_origin.z), 1.0f);
        Gizmos.DrawWireSphere(new Vector3(m_origin.x + m_maxX, m_origin.y + m_maxY, m_origin.z), 1.0f);

        // Left Front
        Gizmos.DrawWireSphere(new Vector3(m_origin.x - m_maxX, m_origin.y - m_maxY, m_origin.z + m_maxZ), 1.0f);
        Gizmos.DrawWireSphere(new Vector3(m_origin.x - m_maxX, m_origin.y, m_origin.z + m_maxZ), 1.0f);
        Gizmos.DrawWireSphere(new Vector3(m_origin.x - m_maxX, m_origin.y + m_maxY, m_origin.z + m_maxZ), 1.0f);

        // Right Front
        Gizmos.DrawWireSphere(new Vector3(m_origin.x + m_maxX, m_origin.y - m_maxY, m_origin.z + m_maxZ), 1.0f);
        Gizmos.DrawWireSphere(new Vector3(m_origin.x + m_maxX, m_origin.y, m_origin.z + m_maxZ), 1.0f);
        Gizmos.DrawWireSphere(new Vector3(m_origin.x + m_maxX, m_origin.y + m_maxY, m_origin.z + m_maxZ), 1.0f);

        // Left
        Gizmos.DrawWireSphere(new Vector3(m_origin.x, m_origin.y - m_maxY, m_origin.z - m_maxZ), 1.0f);
        Gizmos.DrawWireSphere(new Vector3(m_origin.x, m_origin.y, m_origin.z - m_maxZ), 1.0f);
        Gizmos.DrawWireSphere(new Vector3(m_origin.x, m_origin.y + m_maxY, m_origin.z - m_maxZ), 1.0f);

        // Right
        Gizmos.DrawWireSphere(new Vector3(m_origin.x, m_origin.y - m_maxY, m_origin.z + m_maxZ), 1.0f);
        Gizmos.DrawWireSphere(new Vector3(m_origin.x, m_origin.y, m_origin.z + m_maxZ), 1.0f);
        Gizmos.DrawWireSphere(new Vector3(m_origin.x, m_origin.y + m_maxY, m_origin.z + m_maxZ), 1.0f);

        Gizmos.color = Color.blue;

        Gizmos.DrawWireSphere(m_seekObject.transform.position, m_seekRadius);

        Gizmos.color = Color.red;

        // Left Back 
        Gizmos.DrawWireSphere(new Vector3(m_origin.x - (m_maxX / 2.0f), m_origin.y - (m_maxY / 2.0f), m_origin.z - (m_maxZ / 2.0f)), 1.0f);
        Gizmos.DrawWireSphere(new Vector3(m_origin.x - (m_maxX / 2.0f), m_origin.y, m_origin.z - (m_maxZ / 2.0f)), 1.0f);
        Gizmos.DrawWireSphere(new Vector3(m_origin.x - (m_maxX / 2.0f), m_origin.y + (m_maxY / 2.0f), m_origin.z - (m_maxZ / 2.0f)), 1.0f);

        // Right Back
        Gizmos.DrawWireSphere(new Vector3(m_origin.x + (m_maxX / 2.0f), m_origin.y - (m_maxY / 2.0f), m_origin.z - (m_maxZ / 2.0f)), 1.0f);
        Gizmos.DrawWireSphere(new Vector3(m_origin.x + (m_maxX / 2.0f), m_origin.y, m_origin.z - (m_maxZ / 2.0f)), 1.0f);
        Gizmos.DrawWireSphere(new Vector3(m_origin.x + (m_maxX / 2.0f), m_origin.y + (m_maxY / 2.0f), m_origin.z - (m_maxZ / 2.0f)), 1.0f);

        // Left Middle
        Gizmos.DrawWireSphere(new Vector3(m_origin.x - (m_maxX / 2.0f), m_origin.y - (m_maxY / 2.0f), m_origin.z), 1.0f);
        Gizmos.DrawWireSphere(new Vector3(m_origin.x - (m_maxX / 2.0f), m_origin.y, m_origin.z), 1.0f);
        Gizmos.DrawWireSphere(new Vector3(m_origin.x - (m_maxX / 2.0f), m_origin.y + (m_maxY / 2.0f), m_origin.z), 1.0f);

        // Right Middle
        Gizmos.DrawWireSphere(new Vector3(m_origin.x + (m_maxX / 2.0f), m_origin.y - (m_maxY / 2.0f), m_origin.z), 1.0f);
        Gizmos.DrawWireSphere(new Vector3(m_origin.x + (m_maxX / 2.0f), m_origin.y, m_origin.z), 1.0f);
        Gizmos.DrawWireSphere(new Vector3(m_origin.x + (m_maxX / 2.0f), m_origin.y + (m_maxY / 2.0f), m_origin.z), 1.0f);

        // Left Front
        Gizmos.DrawWireSphere(new Vector3(m_origin.x - (m_maxX / 2.0f), m_origin.y - (m_maxY / 2.0f), m_origin.z + (m_maxZ / 2.0f)), 1.0f);
        Gizmos.DrawWireSphere(new Vector3(m_origin.x - (m_maxX / 2.0f), m_origin.y, m_origin.z + (m_maxZ / 2.0f)), 1.0f);
        Gizmos.DrawWireSphere(new Vector3(m_origin.x - (m_maxX / 2.0f), m_origin.y + (m_maxY / 2.0f), m_origin.z + (m_maxZ / 2.0f)), 1.0f);

        // Right Front
        Gizmos.DrawWireSphere(new Vector3(m_origin.x + (m_maxX / 2.0f), m_origin.y - (m_maxY / 2.0f), m_origin.z + (m_maxZ / 2.0f)), 1.0f);
        Gizmos.DrawWireSphere(new Vector3(m_origin.x + (m_maxX / 2.0f), m_origin.y, m_origin.z + (m_maxZ / 2.0f)), 1.0f);
        Gizmos.DrawWireSphere(new Vector3(m_origin.x + (m_maxX / 2.0f), m_origin.y + (m_maxY / 2.0f), m_origin.z + (m_maxZ / 2.0f)), 1.0f);

        // Left
        Gizmos.DrawWireSphere(new Vector3(m_origin.x, m_origin.y - (m_maxY / 2.0f), m_origin.z - (m_maxZ / 2.0f)), 1.0f);
        Gizmos.DrawWireSphere(new Vector3(m_origin.x, m_origin.y, m_origin.z - (m_maxZ / 2.0f)), 1.0f);
        Gizmos.DrawWireSphere(new Vector3(m_origin.x, m_origin.y + (m_maxY / 2.0f), m_origin.z - (m_maxZ / 2.0f)), 1.0f);

        // Right
        Gizmos.DrawWireSphere(new Vector3(m_origin.x, m_origin.y - (m_maxY / 2.0f), m_origin.z + (m_maxZ / 2.0f)), 1.0f);
        Gizmos.DrawWireSphere(new Vector3(m_origin.x, m_origin.y, m_origin.z + (m_maxZ / 2.0f)), 1.0f);
        Gizmos.DrawWireSphere(new Vector3(m_origin.x, m_origin.y + (m_maxY / 2.0f), m_origin.z + (m_maxZ / 2.0f)), 1.0f);
    }
#endif
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class UnitPursue : MonoBehaviour
{
    private Rigidbody m_rigidBody;
    public Rigidbody m_target;

    public float m_force;

    public float m_maxVelocity;

    public BludgerAI m_bludgerAI;
    
	// Use this for initialization
	void Start ()
	{
	    m_rigidBody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
	    if (m_target != null)
	    {
	        Vector3 direction = m_target.position - m_rigidBody.position;

	        float force = (((m_bludgerAI.m_pursueDistance - direction.magnitude) / m_bludgerAI.m_pursueDistance) * m_force) + 1;

            m_rigidBody.AddForce(direction.normalized * force);
            
            Globals.CapVelocity(ref m_rigidBody, m_maxVelocity);
	    }
	}
}

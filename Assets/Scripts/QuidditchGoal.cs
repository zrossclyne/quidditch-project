﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuidditchGoal : MonoBehaviour
{
    public MatchHandler m_matchHandler;

    private PlayerTeam m_scoringTeam;

    private bool m_possibleGoal;
    private bool m_inFront;
    private bool m_inBack;

    public QuidditchGoalPart m_frontPart;
    public QuidditchGoalPart m_backPart;

    // Use this for initialization
    void Start ()
    {
        if (m_frontPart != null)
        {
            m_frontPart.CollisionEnter += CollisionStart;
            m_frontPart.CollisionExit += CollisionEnd;
        }

        if (m_backPart != null)
        {
            m_backPart.CollisionEnter += CollisionStart;
            m_backPart.CollisionExit += CollisionEnd;
        }

        if (transform.parent.name.Equals("[Team 1]"))
	    {
	        m_scoringTeam = PlayerTeam.Team_1;
	    }
	    else
	    {
	        m_scoringTeam = PlayerTeam.Team_2;
	    }
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	public void CollisionStart(Collider collider)
	{
	    string colliderName = collider.transform.name;

	    if (colliderName.Contains("Front"))
	    {
            m_inFront = true;

	        if (!m_inBack)
	        {
	            m_possibleGoal = true;
	        }
	        else
	        {
                m_possibleGoal = false;
	        }
	    }

	    if (colliderName.Contains("Back"))
	    {
	        m_inBack = true;
	    }
	}

	public void CollisionEnd(Collider collider)
	{
	    string colliderName = collider.transform.name;
        
	    if (colliderName.Contains("Front"))
	    {
	        m_inFront = false;
	    }

	    if (colliderName.Contains("Back"))
	    {
	        m_inBack = false;

	        if (m_possibleGoal && !m_inFront)
	        {
	            // Goal was scored
                m_matchHandler.TeamGoal(m_scoringTeam);

	            m_possibleGoal = false;
	        }
	    }
    }
}

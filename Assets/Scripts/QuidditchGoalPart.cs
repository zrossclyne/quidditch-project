﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuidditchGoalPart : MonoBehaviour 
{
    public delegate void CollisionEvent(Collider collider);

    public event CollisionEvent CollisionEnter;

    public event CollisionEvent CollisionExit;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

    void OnTriggerEnter(Collider collider)
    {
        if (CollisionEnter != null)
        {
            CollisionEnter.Invoke(GetComponent<BoxCollider>());
        }
    }

    void OnTriggerExit(Collider collider)
    {
        if (CollisionExit != null)
        {
            CollisionExit.Invoke(GetComponent<BoxCollider>());
        }
    }
}

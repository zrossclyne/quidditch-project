﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeatersBat : MonoBehaviour
{
    public float speed;
    public Vector3 lastPosition;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
	    speed = (((transform.localPosition - lastPosition).magnitude) / Time.deltaTime);
	    lastPosition = transform.localPosition;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutOfBoundsController : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void FixedUpdate()
    {

    }

    void OnTriggerExit(Collider collider)
    {
        Player player = collider.GetComponentInParent<Player>();

        if (player != null)
        {
            player.m_outOfBounds = true;
        }
    }

    void OnTriggerEnter(Collider collider)
    {
        Player player = collider.GetComponentInParent<Player>();

        if (player != null)
        {
            player.m_outOfBounds = false;
        }
    }
}

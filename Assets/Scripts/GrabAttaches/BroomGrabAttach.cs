﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;
using VRTK.GrabAttachMechanics;
using VRTK.SecondaryControllerGrabActions;

public class BroomGrabAttach : VRTK_ChildOfControllerGrabAttach
{
    public bool m_swapHands = true;

    public override void StopGrab(bool applyGrabbingObjectVelocity)
    {
        GameObject interactGrabObject = grabbedObjectScript.GetSecondaryGrabbingObject();

        if (m_swapHands)
        {
            if (interactGrabObject != null)
            {
                VRTK_InteractGrab interactGrab = interactGrabObject.GetComponent<VRTK_InteractGrab>();

                interactGrab.ForceRelease();
                interactGrab.AttemptGrab(true);
            }
        }

        ReleaseObject(applyGrabbingObjectVelocity);
        base.StopGrab(applyGrabbingObjectVelocity);
    }

    public override bool StartGrab(GameObject grabbingObject, GameObject givenGrabbedObject, Rigidbody givenControllerAttachPoint)
    {
        Debug.Log(grabbingObject);

        return base.StartGrab(grabbingObject, givenGrabbedObject, givenControllerAttachPoint);
    }
}

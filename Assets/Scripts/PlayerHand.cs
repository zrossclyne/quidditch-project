﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHand : MonoBehaviour
{
    public Vector3 m_direction;
    public float m_speed;

    private Vector3 m_lastPosition;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
	    m_direction = transform.localPosition - m_lastPosition;
        
        m_speed = m_direction.magnitude / Time.deltaTime;
        
        m_lastPosition = transform.localPosition;
	}
}

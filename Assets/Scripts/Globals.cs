﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public static class Globals
{
    public static float dragStrength = 20.0f;

    public static GameObject broomGrabbed = null;
    public static VRTK_InteractGrab primaryHand = null;

    public static float velocityScalar = 1.0f;

    public static void CapVelocity(ref Rigidbody rigidBody, float maxVelocity)
    {
        if (rigidBody.velocity.sqrMagnitude > Mathf.Pow(maxVelocity, 2))
        {
            float difference = rigidBody.velocity.magnitude - maxVelocity;

            rigidBody.AddForce(rigidBody.velocity.normalized * -difference);
        }
    }

    public static void ApplyDrag(ref Rigidbody rigidBody)
    {
        rigidBody.AddForce(rigidBody.velocity.normalized * -dragStrength);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Policy;
using UnityEngine;
using VRTK;

public enum FlyingType
{
	Lean,
	Button
}

public class FlyingBehaviour : MonoBehaviour
{
	public Transform m_playerHeadTransform;

	public Transform m_broomCenter;
	public Transform m_frontTransform;

	public GameObject m_flyingContainerPrefab;

	public VRTK_InteractableObject m_interactableObject;

	public Rigidbody m_Rigidbody;

	public FlyingType m_flyingType;

	private GameObject m_flyingContainer;

	// This will act as max acceleration when in lean mode
	public float m_acceleration = 40.0f;

	public float m_velocity;
	public float m_maxVelocity;

	public float m_windStrength = 10.0f;

	public float m_defaultDistance = float.MinValue;

	private int m_grabCount = 0;

	public float m_playerHeight = 0;

	// Use this for initialization
	void Start ()
	{
		m_flyingContainer = Instantiate(m_flyingContainerPrefab);

		m_interactableObject.InteractableObjectGrabbed += BroomObjectGrabbed;

		m_interactableObject.InteractableObjectUngrabbed += BroomObjectUngrabbed;
	}

	bool IsBroomGrabbed()
	{
		return Globals.broomGrabbed != null;
	}

	private void BroomObjectUngrabbed(object sender, InteractableObjectEventArgs e)
	{
		if (--m_grabCount == 0)
		{
			Globals.broomGrabbed = null;
			Globals.primaryHand = null;
		}
	}

	private void BroomObjectGrabbed(object sender, InteractableObjectEventArgs e)
	{
		if (Globals.broomGrabbed != null && Globals.broomGrabbed.name != m_interactableObject.name)
		{
			// force the player to stop holding any other broom
			// still allow 2 hand holding of 1 broom
			Globals.primaryHand.ForceRelease();
			Globals.broomGrabbed = null;
			Globals.primaryHand = null;
		}

		if (Globals.broomGrabbed == null)
		{
			Globals.broomGrabbed = m_interactableObject.gameObject;
			Globals.primaryHand = e.interactingObject.GetComponent<VRTK_InteractGrab>();
		}

		++m_grabCount;

		if (m_grabCount <= 1)
		{
			m_playerHeight = m_playerHeadTransform.parent.localPosition.y;

			m_defaultDistance = Vector3.Distance(m_playerHeadTransform.position, m_frontTransform.position);
		}
	}

	// Update is called once per frame
	void FixedUpdate ()
	{
		m_velocity = m_Rigidbody.velocity.magnitude;

		Vector3 movementForce = Vector3.zero;

		if (m_interactableObject.IsGrabbed())
		{
			// Should fly
			if (m_flyingType == FlyingType.Button)
			{
				if (OVRInput.Get(OVRInput.Button.Three))
				{
					movementForce = m_interactableObject.transform.forward * m_acceleration;
				}
				else if (OVRInput.Get(OVRInput.Button.One))
				{
					movementForce = m_interactableObject.transform.forward * -m_acceleration;
				}
			}
			else if (m_flyingType == FlyingType.Lean)
			{
				float headToCenterDist = Vector3.Distance(m_playerHeadTransform.position, m_broomCenter.position);
				float headToFrontDist = Vector3.Distance(m_playerHeadTransform.position, m_frontTransform.position);

				if (headToCenterDist > headToFrontDist)
				{
					// Head is closer to the front so is leaning forward
					if (headToFrontDist > m_defaultDistance)
					{
                        // This line breaks the lean movement entirely
						//headToFrontDist = m_defaultDistance;
					}
				}

				float distancePercentage = headToFrontDist / m_defaultDistance; // How close is the player's head to the front of the broom (or their second hand)

				float acceleration = m_acceleration - (m_acceleration * distancePercentage);

				if (acceleration > m_acceleration)
				{
					acceleration = m_acceleration;
				}

				movementForce = m_interactableObject.transform.forward * acceleration;
			}

			m_Rigidbody.AddRelativeForce(movementForce);

			if (m_Rigidbody.velocity.magnitude <= 0.05f)
			{
				m_Rigidbody.velocity = Vector3.zero;
			}

			if (m_Rigidbody.velocity.magnitude > 0)
			{
				// Apply external forces

				// Apply wind
                Globals.ApplyDrag(ref m_Rigidbody);

				// Cap velocity
				Globals.CapVelocity(ref m_Rigidbody, m_maxVelocity);

				float turnAngle = Vector3.Angle(m_Rigidbody.velocity.normalized, m_interactableObject.transform.forward);

				if (turnAngle > 10.0f)
				{
					Vector3 turnDirection = (m_interactableObject.transform.forward * m_acceleration) - m_Rigidbody.velocity;

					float turnForceWeight = (turnDirection.magnitude / 2.0f) > m_Rigidbody.velocity.magnitude ? m_Rigidbody.velocity.magnitude : (turnDirection.magnitude / 2.0f);

					Vector3 turnForce = turnDirection.normalized * turnForceWeight;

					m_Rigidbody.AddForce(turnForce);
				}
			}
		}
		else if (!IsBroomGrabbed())
		{
            // Stop the player from moving when the broom is not being held.
			m_Rigidbody.velocity = Vector3.zero;
		}
	}
}

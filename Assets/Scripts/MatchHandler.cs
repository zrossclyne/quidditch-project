﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vexe.Runtime.Types;
using VFWExamples;

public class Score
{
    [Show]
    public Dictionary<PlayerTeam, int> teams = new Dictionary<PlayerTeam, int>();

    public Score()
    {
        teams = new Dictionary<PlayerTeam, int>();
    }

    public void SetScore(PlayerTeam team, int score)
    {
        teams[team] = score;
    }
}

public class MatchHandler : BaseBehaviour
{
    private bool m_isPlaying;
    
    public Score m_score = new Score();

    public GameObject m_quaffle;
    
    // Use this for initialization
    void Start ()
	{
	    m_score.SetScore(PlayerTeam.Team_1, 0);

	    m_score.SetScore(PlayerTeam.Team_2, 0);

	    m_isPlaying = true;
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

    public void TeamGoal(PlayerTeam team)
    {
        AddScore(team, 10);

        Transform keeperPosition = GetPlayerPosition(team == PlayerTeam.Team_1 ? PlayerTeam.Team_2 : PlayerTeam.Team_1, PlayerType.Keeper);

        m_quaffle.transform.position = keeperPosition.position + (keeperPosition.forward * 0.1f);

        Rigidbody quaffleRigidBody = m_quaffle.GetComponent<Rigidbody>();

        quaffleRigidBody.velocity = Vector3.zero;
        quaffleRigidBody.useGravity = false;
    }

    public void TeamSnitch(PlayerTeam team)
    {
        AddScore(team, 150);

        m_isPlaying = false;
    }

    public void AddScore(PlayerTeam team, int points)
    {
        m_score.teams[team] += points;
    }

    private Transform GetPlayerPosition(PlayerTeam team, PlayerType playerType, int index = 0)
    {
        Player[] players = GameObject.FindObjectsOfType<Player>();

        foreach (Player player in players)
        {
            if (player.m_playerTeam == team)
            {
                if (player.m_playerType == playerType && player.m_playerPosition == index)
                {
                    return player.transform;
                }
            }
        }

        return null;
    }
}

﻿//
// Weather Maker for Unity
// (c) 2016 Digital Ruby, LLC
// Source code may be used for personal or commercial projects.
// Source code may NOT be redistributed or sold.
// 
// *** A NOTE ABOUT PIRACY ***
// 
// If you got this asset off of leak forums or any other horrible evil pirate site, please consider buying it from the Unity asset store at https ://www.assetstore.unity3d.com/en/#!/content/60955?aid=1011lGnL. This asset is only legally available from the Unity Asset Store.
// 
// I'm a single indie dev supporting my family by spending hundreds and thousands of hours on this and other assets. It's very offensive, rude and just plain evil to steal when I (and many others) put so much hard work into the software.
// 
// Thank you.
//
// *** END NOTE ABOUT PIRACY ***
//

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DigitalRuby.WeatherMaker
{
    [CreateAssetMenu(fileName = "WeatherMakerCloudProfile", menuName = "WeatherMaker/Cloud Profile", order = 2)]
    public class WeatherMakerCloudProfileScript : ScriptableObject
    {
        [Header("Clouds - Noise")]
        [Tooltip("Texture (layer 1, bottom) for cloud noise - use single channel texture only.")]
        public Texture2D CloudNoise;

        [Tooltip("Texture (layer 2, middle) for cloud noise - use single channel texture only.")]
        public Texture2D CloudNoise2;

        [Tooltip("Texture (layer 3, middle) for cloud noise - use single channel texture only.")]
        public Texture2D CloudNoise3;

        [Tooltip("Texture (layer 4, top) for cloud noise - use single channel texture only.")]
        public Texture2D CloudNoise4;

        /*
        [Tooltip("Cloud sample count, layer 1")]
        [Range(1, 100)]
        public int CloudSampleCount = 1;

        [Tooltip("Cloud sample count, layer 1")]
        [Range(1, 100)]
        public int CloudSampleCount2 = 1;

        [Tooltip("Cloud sample count, layer 1")]
        [Range(1, 100)]
        public int CloudSampleCount3 = 1;

        [Tooltip("Cloud sample count, layer 1")]
        [Range(1, 100)]
        public int CloudSampleCount4 = 1;

        [Tooltip("Cloud sample step multiplier, layer 1, up to 4 octaves.")]
        public Vector4 CloudSampleStepMultiplier = new Vector4(50.0f, 50.0f, 50.0f, 50.0f);

        [Tooltip("Cloud sample step multiplier, layer 2, up to 4 octaves.")]
        public Vector4 CloudSampleStepMultiplier2 = new Vector4(50.0f, 50.0f, 50.0f, 50.0f);

        [Tooltip("Cloud sample step multiplier, layer 3, up to 4 octaves.")]
        public Vector4 CloudSampleStepMultiplier3 = new Vector4(50.0f, 50.0f, 50.0f, 50.0f);

        [Tooltip("Cloud sample step multiplier, layer 4, up to 4 octaves.")]
        public Vector4 CloudSampleStepMultiplier4 = new Vector4(50.0f, 50.0f, 50.0f, 50.0f);

        [Tooltip("Cloud sample dither magic, controls appearance of clouds through ray march, layer 1.")]
        public Vector4 CloudSampleDitherMagic;

        [Tooltip("Cloud sample dither magic, controls appearance of clouds through ray march, layer 2.")]
        public Vector4 CloudSampleDitherMagic2;

        [Tooltip("Cloud sample dither magic, controls appearance of clouds through ray march, layer 3.")]
        public Vector4 CloudSampleDitherMagic3;

        [Tooltip("Cloud sample dither magic, controls appearance of clouds through ray march, layer 4.")]
        public Vector4 CloudSampleDitherMagic4;

        [Tooltip("Cloud sample dither intensity, 4 layers (x,y,z,w).")]
        public Vector4 CloudSampleDitherIntensity;
        */

        [Tooltip("Cloud noise scale, layer 1, up to 4 octaves, set to 0 to not process that octave.")]
        public Vector4 CloudNoiseScale = new Vector4(0.0002f, 0.0f, 0.0f, 0.0f);

        [Tooltip("Cloud noise scale, layer 2, up to 4 octaves, set to 0 to not process that octave.")]
        public Vector4 CloudNoiseScale2 = new Vector4(0.0002f, 0.0f, 0.0f, 0.0f);

        [Tooltip("Cloud noise scale, layer 3, up to 4 octaves, set to 0 to not process that octave.")]
        public Vector4 CloudNoiseScale3 = new Vector4(0.0002f, 0.0f, 0.0f, 0.0f);

        [Tooltip("Cloud noise scale, layer 4, up to 4 octaves, set to 0 to not process that octave.")]
        public Vector4 CloudNoiseScale4 = new Vector4(0.0002f, 0.0f, 0.0f, 0.0f);

        [Tooltip("Cloud noise multiplier, up to 4 octaves. Should add up to about 1.")]
        public Vector4 CloudNoiseMultiplier = new Vector4(1.0f, 0.0f, 0.0f, 0.0f);

        [Tooltip("Cloud noise multiplier, layer 2, up to 4 octaves. Should add up to about 1.")]
        public Vector4 CloudNoiseMultiplier2 = new Vector4(1.0f, 0.0f, 0.0f, 0.0f);

        [Tooltip("Cloud noise multiplier, layer 3, up to 4 octaves. Should add up to about 1.")]
        public Vector4 CloudNoiseMultiplier3 = new Vector4(1.0f, 0.0f, 0.0f, 0.0f);

        [Tooltip("Cloud noise multiplier, layer 4, up to 4 octaves. Should add up to about 1.")]
        public Vector4 CloudNoiseMultiplier4 = new Vector4(1.0f, 0.0f, 0.0f, 0.0f);

        [SingleLine("Cloud noise rotation in degrees, layer 1")]
        public RangeOfFloats CloudNoiseRotation;

        [SingleLine("Cloud noise rotation in degrees, layer 2")]
        public RangeOfFloats CloudNoiseRotation2;

        [SingleLine("Cloud noise rotation in degrees, layer 3")]
        public RangeOfFloats CloudNoiseRotation3;

        [SingleLine("Cloud noise rotation in degrees, layer 4")]
        public RangeOfFloats CloudNoiseRotation4;

        [Tooltip("Cloud noise velocity, layer 1")]
        public Vector3 CloudNoiseVelocity;

        [Tooltip("Cloud noise velocity, layer 2")]
        public Vector3 CloudNoiseVelocity2;

        [Tooltip("Cloud noise velocity, layer 3")]
        public Vector3 CloudNoiseVelocity3;

        [Tooltip("Cloud noise velocity, layer 4")]
        public Vector3 CloudNoiseVelocity4;

        [Tooltip("Texture for masking cloud noise, makes clouds visible in only certain parts of the sky, layer 1.")]
        public Texture2D CloudNoiseMask;

        [Tooltip("Texture for masking cloud noise, makes clouds visible in only certain parts of the sky, layer 2.")]
        public Texture2D CloudNoiseMask2;

        [Tooltip("Texture for masking cloud noise, makes clouds visible in only certain parts of the sky, layer 3.")]
        public Texture2D CloudNoiseMask3;

        [Tooltip("Texture for masking cloud noise, makes clouds visible in only certain parts of the sky, layer 4.")]
        public Texture2D CloudNoiseMask4;

        [Tooltip("Cloud noise mask scale, layer 1")]
        [Range(0.000001f, 1.0f)]
        public float CloudNoiseMaskScale = 0.0001f;

        [Tooltip("Cloud noise mask scale, layer 2")]
        [Range(0.000001f, 1.0f)]
        public float CloudNoiseMaskScale2 = 0.0001f;

        [Tooltip("Cloud noise mask scale, layer 3")]
        [Range(0.000001f, 1.0f)]
        public float CloudNoiseMaskScale3 = 0.0001f;

        [Tooltip("Cloud noise mask scale, layer 4")]
        [Range(0.000001f, 1.0f)]
        public float CloudNoiseMaskScale4 = 0.0001f;

        [SingleLine("Cloud noise mask rotation in degrees, layer 1")]
        public RangeOfFloats CloudNoiseMaskRotation;

        [SingleLine("Cloud noise mask rotation in degrees, layer 2")]
        public RangeOfFloats CloudNoiseMaskRotation2;

        [SingleLine("Cloud noise mask rotation in degrees, layer 3")]
        public RangeOfFloats CloudNoiseMaskRotation3;

        [SingleLine("Cloud noise mask rotation in degrees, layer 4")]
        public RangeOfFloats CloudNoiseMaskRotation4;

        [Tooltip("Offset for cloud noise mask, layer 1")]
        public Vector2 CloudNoiseMaskOffset;

        [Tooltip("Offset for cloud noise mask, layer 2")]
        public Vector2 CloudNoiseMaskOffset2;

        [Tooltip("Offset for cloud noise mask, layer 3")]
        public Vector2 CloudNoiseMaskOffset3;

        [Tooltip("Offset for cloud noise mask, layer 4")]
        public Vector2 CloudNoiseMaskOffset4;

        [Tooltip("Cloud noise mask velocity, layer 1")]
        public Vector3 CloudNoiseMaskVelocity;

        [Tooltip("Cloud noise mask velocity, layer 2")]
        public Vector3 CloudNoiseMaskVelocity2;

        [Tooltip("Cloud noise mask velocity, layer 3")]
        public Vector3 CloudNoiseMaskVelocity3;

        [Tooltip("Cloud noise mask velocity, layer 4")]
        public Vector3 CloudNoiseMaskVelocity4;

        [Header("Clouds - Appearance")]
        [Tooltip("Cloud color, for lighting, layer 1")]
        public Color CloudColor = Color.white;

        [Tooltip("Cloud color, for lighting, layer 2")]
        public Color CloudColor2 = Color.white;

        [Tooltip("Cloud color, for lighting, layer 3")]
        public Color CloudColor3 = Color.white;

        [Tooltip("Cloud color, for lighting, layer 4")]
        public Color CloudColor4 = Color.white;

        [Tooltip("Cloud emission color, always emits this color regardless of lighting, layer 1")]
        public Color CloudEmissionColor = Color.clear;

        [Tooltip("Cloud emission color, always emits this color regardless of lighting, layer 2")]
        public Color CloudEmissionColor2 = Color.clear;

        [Tooltip("Cloud emission color, always emits this color regardless of lighting, layer 3")]
        public Color CloudEmissionColor3 = Color.clear;

        [Tooltip("Cloud emission color, always emits this color regardless of lighting, layer 4")]
        public Color CloudEmissionColor4 = Color.clear;

        [Tooltip("Cloud height - only affects where the clouds stop at the horizon, layer 1")]
        [Range(1.0f, 10000.0f)]
        public float CloudHeight = 5000;

        [Tooltip("Cloud height - only affects where the clouds stop at the horizon, layer 2")]
        [Range(1.0f, 10000.0f)]
        public float CloudHeight2 = 5000;

        [Tooltip("Cloud height - only affects where the clouds stop at the horizon, layer 3")]
        [Range(1.0f, 10000.0f)]
        public float CloudHeight3 = 5000;

        [Tooltip("Cloud height - only affects where the clouds stop at the horizon, layer 4")]
        [Range(1.0f, 10000.0f)]
        public float CloudHeight4 = 5000;

        [Tooltip("Cloud cover, controls how many clouds there are, layer 1")]
        [Range(0.0f, 1.0f)]
        public float CloudCover = 0.0f;

        [Tooltip("Cloud cover, controls how many clouds there are, layer 2")]
        [Range(0.0f, 1.0f)]
        public float CloudCover2 = 0.0f;

        [Tooltip("Cloud cover, controls how many clouds there are, layer 3")]
        [Range(0.0f, 1.0f)]
        public float CloudCover3 = 0.0f;

        [Tooltip("Cloud cover, controls how many clouds there are, layer 4")]
        [Range(0.0f, 1.0f)]
        public float CloudCover4 = 0.0f;

        [Tooltip("Cloud density, controls how opaque the clouds are, layer 1")]
        [Range(0.0f, 1.0f)]
        public float CloudDensity = 0.0f;

        [Tooltip("Cloud density, controls how opaque the clouds are, layer 2")]
        [Range(0.0f, 1.0f)]
        public float CloudDensity2 = 0.0f;

        [Tooltip("Cloud density, controls how opaque the clouds are, layer 3")]
        [Range(0.0f, 1.0f)]
        public float CloudDensity3 = 0.0f;

        [Tooltip("Cloud density, controls how opaque the clouds are, layer 4")]
        [Range(0.0f, 1.0f)]
        public float CloudDensity4 = 0.0f;

        [Tooltip("Cloud light absorption. As this approaches 0, all light is absorbed, layer 1")]
        [Range(0.0f, 1.0f)]
        public float CloudLightAbsorption = 0.13f;

        [Tooltip("Cloud light absorption. As this approaches 0, all light is absorbed, layer 2")]
        [Range(0.0f, 1.0f)]
        public float CloudLightAbsorption2 = 0.13f;

        [Tooltip("Cloud light absorption. As this approaches 0, all light is absorbed, layer 3")]
        [Range(0.0f, 1.0f)]
        public float CloudLightAbsorption3 = 0.13f;

        [Tooltip("Cloud light absorption. As this approaches 0, all light is absorbed, layer 4")]
        [Range(0.0f, 1.0f)]
        public float CloudLightAbsorption4 = 0.13f;

        [Tooltip("Cloud sharpness, controls how distinct the clouds are, layer 1")]
        [Range(0.0f, 1.0f)]
        public float CloudSharpness = 0.015f;

        [Tooltip("Cloud sharpness, controls how distinct the clouds are, layer 2")]
        [Range(0.0f, 1.0f)]
        public float CloudSharpness2 = 0.015f;

        [Tooltip("Cloud sharpness, controls how distinct the clouds are, layer 3")]
        [Range(0.0f, 1.0f)]
        public float CloudSharpness3 = 0.015f;

        [Tooltip("Cloud sharpness, controls how distinct the clouds are, layer 4")]
        [Range(0.0f, 1.0f)]
        public float CloudSharpness4 = 0.015f;

        [Tooltip("Cloud pixels with alpha greater than this will cast a shadow. Set to 1 to disable cloud shadows. Layer 1.")]
        [Range(0.0f, 1.0f)]
        public float CloudShadowThreshold = 0.1f;

        [Tooltip("Cloud pixels with alpha greater than this will cast a shadow. Set to 1 to disable cloud shadows. Layer 2.")]
        [Range(0.0f, 1.0f)]
        public float CloudShadowThreshold2 = 0.1f;

        [Tooltip("Cloud pixels with alpha greater than this will cast a shadow. Set to 1 to disable cloud shadows. Layer 3.")]
        [Range(0.0f, 1.0f)]
        public float CloudShadowThreshold3 = 0.1f;

        [Tooltip("Cloud pixels with alpha greater than this will cast a shadow. Set to 1 to disable cloud shadows. Layer 4.")]
        [Range(0.0f, 1.0f)]
        public float CloudShadowThreshold4 = 0.1f;

        [Tooltip("Cloud shadow power. 0 is full power, higher loses power. Layer 1.")]
        [Range(0.0001f, 2.0f)]
        public float CloudShadowPower = 0.5f;

        [Tooltip("Cloud shadow power. 0 is full power, higher loses power. Layer 2.")]
        [Range(0.0001f, 2.0f)]
        public float CloudShadowPower2 = 0.5f;

        [Tooltip("Cloud shadow power. 0 is full power, higher loses power. Layer 3.")]
        [Range(0.0001f, 2.0f)]
        public float CloudShadowPower3 = 0.5f;

        [Tooltip("Cloud shadow power. 0 is full power, higher loses power. Layer 4.")]
        [Range(0.0001f, 2.0f)]
        public float CloudShadowPower4 = 0.5f;

        [Tooltip("Bring clouds down at the horizon at the cost of stretching over the top.")]
        [Range(0.0f, 0.5f)]
        public float CloudRayOffset = 0.2f;

        private const float scaleReducer = 0.1f;

        /// <summary>
        /// Checks whether clouds are enabled
        /// </summary>
        public bool CloudsEnabled { get; private set; }

        /// <summary>
        /// Sum of cloud cover, max of 1
        /// </summary>
        public float CloudCoverTotal { get; private set; }

        /// <summary>
        /// Sum of cloud density, max of 1
        /// </summary>
        public float CloudDensityTotal { get; private set; }

        /// <summary>
        /// Sum of cloud light absorption, max of 1
        /// </summary>
        public float CloudLightAbsorptionTotal { get; private set; }

        public void SetShaderCloudParameters(Material m)
        {
            m.DisableKeyword("ENABLE_CLOUDS");
            m.DisableKeyword("ENABLE_CLOUDS_MASK");

            if (CloudsEnabled)
            {
                m.SetTexture("_CloudNoise", CloudNoise ?? Texture2D.blackTexture);
                m.SetTexture("_CloudNoise2", CloudNoise2 ?? Texture2D.blackTexture);
                m.SetTexture("_CloudNoise3", CloudNoise3 ?? Texture2D.blackTexture);
                m.SetTexture("_CloudNoise4", CloudNoise4 ?? Texture2D.blackTexture);
                //WeatherMakerShaderIds.SetFloatArray(m, "_CloudSampleCount", (float)CloudSampleCount, (float)CloudSampleCount2, (float)CloudSampleCount3, (float)CloudSampleCount4);
                //WeatherMakerShaderIds.SetVectorArray(m, "_CloudSampleStepMultiplier", CloudSampleStepMultiplier, CloudSampleStepMultiplier2, CloudSampleStepMultiplier3, CloudSampleStepMultiplier4);
                WeatherMakerShaderIds.SetColorArray(m, "_CloudColor", CloudColor, CloudColor2, CloudColor3, CloudColor4);
                WeatherMakerShaderIds.SetColorArray(m, "_CloudEmissionColor", CloudEmissionColor, CloudEmissionColor2, CloudEmissionColor3, CloudEmissionColor4);
                float velMult = Time.deltaTime * 0.005f;
                WeatherMakerShaderIds.SetVectorArray(m, "_CloudNoiseVelocity",
                    (cloudNoiseVelocityAccum += (CloudNoiseVelocity * velMult)),
                    (cloudNoiseVelocityAccum2 += (CloudNoiseVelocity2 * velMult)),
                    (cloudNoiseVelocityAccum3 += (CloudNoiseVelocity3 * velMult)),
                    (cloudNoiseVelocityAccum4 += (CloudNoiseVelocity4 * velMult)));
                if (CloudNoiseMask == null)
                {
                    m.EnableKeyword("ENABLE_CLOUDS");
                }
                else
                {
                    m.EnableKeyword("ENABLE_CLOUDS_MASK");
                    m.SetTexture("_CloudNoiseMask", CloudNoiseMask ?? Texture2D.whiteTexture);
                    m.SetTexture("_CloudNoiseMask2", CloudNoiseMask2 ?? Texture2D.whiteTexture);
                    m.SetTexture("_CloudNoiseMask3", CloudNoiseMask3 ?? Texture2D.whiteTexture);
                    m.SetTexture("_CloudNoiseMask4", CloudNoiseMask4 ?? Texture2D.whiteTexture);
                    WeatherMakerShaderIds.SetVectorArray(m, "_CloudNoiseMaskOffset", CloudNoiseMaskOffset, CloudNoiseMaskOffset2, CloudNoiseMaskOffset3, CloudNoiseMaskOffset4);
                    WeatherMakerShaderIds.SetVectorArray(m, "_CloudNoiseMaskVelocity",
                        (cloudNoiseMaskVelocityAccum += (CloudNoiseMaskVelocity * velMult)),
                        (cloudNoiseMaskVelocityAccum2 += (CloudNoiseMaskVelocity2 * velMult)),
                        (cloudNoiseMaskVelocityAccum3 += (CloudNoiseMaskVelocity3 * velMult)),
                        (cloudNoiseMaskVelocityAccum4 += (CloudNoiseMaskVelocity4 * velMult)));
                    WeatherMakerShaderIds.SetFloatArray(m, "_CloudNoiseMaskScale", CloudNoiseMaskScale * scaleReducer, CloudNoiseMaskScale2 * scaleReducer, CloudNoiseMaskScale3 * scaleReducer, CloudNoiseMaskScale4 * scaleReducer);
                    WeatherMakerShaderIds.SetFloatArrayRotation(m, "_CloudNoiseMaskRotation", CloudNoiseMaskRotation.LastValue, CloudNoiseMaskRotation2.LastValue, CloudNoiseMaskRotation3.LastValue, CloudNoiseMaskRotation4.LastValue);
                }
                WeatherMakerShaderIds.SetVectorArray(m, "_CloudNoiseScale", CloudNoiseScale * scaleReducer, CloudNoiseScale2 * scaleReducer, CloudNoiseScale3 * scaleReducer, CloudNoiseScale4 * scaleReducer);
                WeatherMakerShaderIds.SetVectorArray(m, "_CloudNoiseMultiplier", CloudNoiseMultiplier, CloudNoiseMultiplier2, CloudNoiseMultiplier3, CloudNoiseMultiplier4);
                WeatherMakerShaderIds.SetFloatArrayRotation(m, "_CloudNoiseRotation", CloudNoiseRotation.LastValue, CloudNoiseRotation2.LastValue, CloudNoiseRotation3.LastValue, CloudNoiseRotation4.LastValue);
                //WeatherMakerShaderIds.SetFloatArray(m, "_CloudSampleCount", CloudSampleCount, CloudSampleCount2, CloudSampleCount3, CloudSampleCount4);
                //WeatherMakerShaderIds.SetVectorArray(m, "_CloudSampleDitherMagic", CloudSampleDitherMagic, CloudSampleDitherMagic2, CloudSampleDitherMagic3, CloudSampleDitherMagic4);
                //WeatherMakerShaderIds.SetFloatArray(m, "_CloudSampleDitherIntensity", CloudSampleDitherIntensity.x, CloudSampleDitherIntensity.y, CloudSampleDitherIntensity.z, CloudSampleDitherIntensity.w);
                WeatherMakerShaderIds.SetFloatArray(m, "_CloudHeight", CloudHeight, CloudHeight2, CloudHeight3, CloudHeight4);
                WeatherMakerShaderIds.SetFloatArray(m, "_CloudCover", CloudCover, CloudCover2, CloudCover3, CloudCover4);
                WeatherMakerShaderIds.SetFloatArray(m, "_CloudDensity", CloudDensity, CloudDensity2, CloudDensity3, CloudDensity4);
                WeatherMakerShaderIds.SetFloatArray(m, "_CloudLightAbsorption", CloudLightAbsorption, CloudLightAbsorption2, CloudLightAbsorption3, CloudLightAbsorption4);
                WeatherMakerShaderIds.SetFloatArray(m, "_CloudSharpness", CloudSharpness, CloudSharpness2, CloudSharpness3, CloudSharpness4);
                WeatherMakerShaderIds.SetFloatArray(m, "_CloudShadowThreshold", CloudShadowThreshold, CloudShadowThreshold2, CloudShadowThreshold3, CloudShadowThreshold4);
                float shadowDotPower = Mathf.Clamp(Mathf.Pow(3.0f * Vector3.Dot(Vector3.down, WeatherMakerScript.Instance.Sun.Transform.forward), 0.5f), 0.0f, 1.0f);
                float shadowPower = Mathf.Lerp(2.0f, CloudShadowPower, shadowDotPower);
                float shadowPower2 = Mathf.Lerp(2.0f, CloudShadowPower2, shadowDotPower);
                float shadowPower3 = Mathf.Lerp(2.0f, CloudShadowPower3, shadowDotPower);
                float shadowPower4 = Mathf.Lerp(2.0f, CloudShadowPower4, shadowDotPower);
                WeatherMakerShaderIds.SetFloatArray(m, "_CloudShadowPower", shadowPower, shadowPower2, shadowPower3, shadowPower4);
                m.SetFloat("_CloudRayOffset", CloudRayOffset);
                int cloudLayerStart;
                if (CloudNoise4 != null)
                {
                    cloudLayerStart = 3;
                }
                else if (CloudNoise3 != null)
                {
                    cloudLayerStart = 2;
                }
                else if (CloudNoise2 != null)
                {
                    cloudLayerStart = 1;
                }
                else
                {
                    cloudLayerStart = 0;
                }
                m.SetInt("_CloudLayerStart", cloudLayerStart);

#if UNITY_EDITOR

                if (Application.isPlaying)
                {

#endif

                    float cover = CloudCoverTotal * (1.5f - CloudLightAbsorptionTotal);
                    float sunIntensityMultiplier = Mathf.Clamp(1.0f - (CloudDensityTotal * 0.5f), 0.0f, 1.0f);
                    WeatherMakerScript.Instance.DayNightScript.DirectionalLightIntensityMultipliers["WeatherMakerSkySphereScript"] = sunIntensityMultiplier;
                    float sunShadowMultiplier = Mathf.Lerp(1.0f, 0.0f, Mathf.Clamp(((CloudDensityTotal + cover) * 0.85f), 0.0f, 1.0f));
                    WeatherMakerScript.Instance.DayNightScript.DirectionalLightShadowIntensityMultipliers["WeatherMakerSkySphereScript"] = sunShadowMultiplier;

#if UNITY_EDITOR

                }

#endif

            }
            else
            {

#if UNITY_EDITOR

                if (Application.isPlaying)
                {

#endif

                    WeatherMakerScript.Instance.DayNightScript.DirectionalLightIntensityMultipliers["WeatherMakerFullScreenCloudsScript"] = 1.0f;
                    WeatherMakerScript.Instance.DayNightScript.DirectionalLightShadowIntensityMultipliers["WeatherMakerFullScreenCloudsScript"] = 1.0f;

#if UNITY_EDITOR

                }

#endif

            }
        }

        private Vector3 cloudNoiseVelocityAccum;
        private Vector3 cloudNoiseVelocityAccum2;
        private Vector3 cloudNoiseVelocityAccum3;
        private Vector3 cloudNoiseVelocityAccum4;
        private Vector3 cloudNoiseMaskVelocityAccum;
        private Vector3 cloudNoiseMaskVelocityAccum2;
        private Vector3 cloudNoiseMaskVelocityAccum3;
        private Vector3 cloudNoiseMaskVelocityAccum4;

        public void Update()
        {
            CloudsEnabled =
            (
                (CloudNoise != null && CloudColor.a > 0.0f && CloudCover > 0.0f) ||
                (CloudNoise2 != null && CloudColor2.a > 0.0f && CloudCover2 > 0.0f) ||
                (CloudNoise3 != null && CloudColor3.a > 0.0f && CloudCover3 > 0.0f) ||
                (CloudNoise4 != null && CloudColor4.a > 0.0f && CloudCover4 > 0.0f)
            );
            CloudCoverTotal = Mathf.Min(1.0f, (CloudCover + CloudCover2 + CloudCover3 + CloudCover4));
            CloudDensityTotal = Mathf.Min(1.0f, (CloudCover * CloudDensity) + (CloudCover2 * CloudDensity2) + (CloudCover3 * CloudDensity3) + (CloudCover4 * CloudDensity4));
            CloudLightAbsorptionTotal = Mathf.Min(1.0f, (CloudCover * CloudLightAbsorption) + (CloudCover2 * CloudLightAbsorption2) + (CloudCover3 * CloudLightAbsorption3) + (CloudCover4 * CloudLightAbsorption4));
        }

        public WeatherMakerCloudProfileScript Clone()
        {
            WeatherMakerCloudProfileScript clone = ScriptableObject.Instantiate<WeatherMakerCloudProfileScript>(this);
            CopyStateTo(clone);
            return clone;
        }

        public void CopyStateTo(WeatherMakerCloudProfileScript clone)
        {
            clone.cloudNoiseVelocityAccum = this.cloudNoiseVelocityAccum;
            clone.cloudNoiseVelocityAccum2 = this.cloudNoiseVelocityAccum2;
            clone.cloudNoiseVelocityAccum3 = this.cloudNoiseVelocityAccum3;
            clone.cloudNoiseVelocityAccum4 = this.cloudNoiseVelocityAccum4;
            clone.cloudNoiseMaskVelocityAccum = this.cloudNoiseMaskVelocityAccum;
            clone.cloudNoiseMaskVelocityAccum2 = this.cloudNoiseMaskVelocityAccum2;
            clone.cloudNoiseMaskVelocityAccum3 = this.cloudNoiseMaskVelocityAccum3;
            clone.cloudNoiseMaskVelocityAccum4 = this.cloudNoiseMaskVelocityAccum4;
            clone.CloudCoverTotal = this.CloudCoverTotal;
            clone.CloudDensityTotal = this.CloudDensityTotal;
            clone.CloudLightAbsorptionTotal = this.CloudLightAbsorptionTotal;
            clone.CloudsEnabled = this.CloudsEnabled;
        }
    }
}

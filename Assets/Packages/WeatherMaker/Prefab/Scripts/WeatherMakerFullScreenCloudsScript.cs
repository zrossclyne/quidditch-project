﻿//
// Weather Maker for Unity
// (c) 2016 Digital Ruby, LLC
// Source code may be used for personal or commercial projects.
// Source code may NOT be redistributed or sold.
// 
// *** A NOTE ABOUT PIRACY ***
// 
// If you got this asset off of leak forums or any other horrible evil pirate site, please consider buying it from the Unity asset store at https ://www.assetstore.unity3d.com/en/#!/content/60955?aid=1011lGnL. This asset is only legally available from the Unity Asset Store.
// 
// I'm a single indie dev supporting my family by spending hundreds and thousands of hours on this and other assets. It's very offensive, rude and just plain evil to steal when I (and many others) put so much hard work into the software.
// 
// Thank you.
//
// *** END NOTE ABOUT PIRACY ***
//

// uncomment to test cloud animations in editor mode
// #define ANIMATE_EDITOR_CLOUD_PROFILE_CHANGES

using UnityEngine;
using UnityEngine.Rendering;

using System;
using System.Collections.Generic;

namespace DigitalRuby.WeatherMaker
{
    public class WeatherMakerFullScreenCloudsScript : MonoBehaviour
    {
        [Header("Full Screen Clouds - Rendering")]
        [Tooltip("Cloud shadow projector script")]
        public WeatherMakerCloudShadowProjectorScript CloudShadowProjectorScript;

        [Tooltip("Cloud rendering material.")]
        public Material Material;

        [Tooltip("Material to blit the full screen clouds.")]
        public Material FullScreenMaterial;

        [Tooltip("Down sample scale.")]
        [Range(0.01f, 1.0f)]
        public float DownSampleScale = 1.0f;

        [Tooltip("Blur Material.")]
        public Material BlurMaterial;

        [Tooltip("Blur Shader Type.")]
        public BlurShaderType BlurShader;

        [Tooltip("Render Queue")]
        public CameraEvent RenderQueue = CameraEvent.BeforeImageEffectsOpaque;

        [Tooltip("Cloud profile")]
        [SerializeField]
        private WeatherMakerCloudProfileScript _CloudProfile;


#if UNITY_EDITOR

        private WeatherMakerCloudProfileScript lastProfile;

#endif

        public WeatherMakerCloudProfileScript CloudProfile
        {
            get { return _CloudProfile; }
            set { ShowCloudsAnimated(5.0f, value); }
        }

        [Tooltip("Used to block lens flare if clouds are over the sun. Just needs to be a sphere collider.")]
        public GameObject CloudLensFlareBlocker;

        private WeatherMakerFullScreenEffect effect;

        private void UpdateShaderProperties(WeatherMakerCommandBuffer b)
        {
            SetShaderCloudParameters(b.Material);
        }

        private void CopyCurrentProfile()
        {

#if UNITY_EDITOR

            if (Application.isPlaying)
            {

#endif

                if (_CloudProfile != null)
                {
                    _CloudProfile = _CloudProfile.Clone();
                    _CloudProfile.name = _CloudProfile.name.Replace("(Clone)", string.Empty).Trim();
                    _CloudProfile.name = "(Clone)" + _CloudProfile.name;
                }

#if UNITY_EDITOR

            }

#endif

        }

        private void UpdateLensFlare(Camera c)
        {
            if (WeatherMakerScript.Instance.Sun == null)
            {
                return;
            }
            LensFlare flare = WeatherMakerScript.Instance.Sun.Transform.GetComponent<LensFlare>();
            if (flare == null)
            {
                return;
            }
            if (CloudProfile.CloudCover < 0.5f)
            {
                CloudLensFlareBlocker.SetActive(false);
            }
            else
            {
                CloudLensFlareBlocker.SetActive(true);
                Vector3 toSun = (c.transform.position - WeatherMakerScript.Instance.Sun.Transform.position).normalized;
                CloudLensFlareBlocker.transform.position = c.transform.position + (toSun * 16.0f);
            }
        }

        private void Awake()
        {
            if (_CloudProfile == null)
            {
                _CloudProfile = ScriptableObject.CreateInstance<WeatherMakerCloudProfileScript>();
                _CloudProfile.name = "(Clone)NullProfile";
            }
        }

        private void Start()
        {
            effect = new WeatherMakerFullScreenEffect
            {
                CommandBufferName = "WeatherMakerFullScreenCloudsScript",
                DownsampleRenderBufferTextureName = "_MainTex2",
                RenderQueue = RenderQueue,
                ZTest = CompareFunction.LessEqual
            };
            CopyCurrentProfile();
        }

        private void Update()
        {

#if UNITY_EDITOR && ANIMATE_EDITOR_CLOUD_PROFILE_CHANGES

            // detect changes in editor mode
            if (lastProfile != _CloudProfile)
            {
                WeatherMakerCloudProfileScript newProfile = _CloudProfile;
                _CloudProfile = lastProfile ?? _CloudProfile;
                CloudProfile = newProfile;
                lastProfile = CloudProfile;
            }

#endif

            CloudProfile.Update();
        }

        private void LateUpdate()
        {
            if (CloudProfile == null)
            {
                return;
            }
            effect.Material = Material;
            effect.BlitMaterial = FullScreenMaterial;
            effect.BlurMaterial = BlurMaterial;
            effect.BlurShaderType = BlurShader;
            effect.DownsampleScale = DownSampleScale;
            effect.DownsampleRenderBufferScale = 0.0f;
            effect.UpdateMaterialProperties = UpdateShaderProperties;
            effect.Enabled = CloudProfile.CloudsEnabled;
            effect.LateUpdate();
        }

        private void OnDestroy()
        {
            if (effect != null)
            {
                effect.Dispose();
            }
        }

        private void OnDisable()
        {
            if (effect != null)
            {
                effect.Dispose();
            }
        }

        internal void SetShaderCloudParameters(Material m)
        {
            CloudProfile.SetShaderCloudParameters(m);
        }

        public void PreCullCamera(Camera camera)
        {
            if (effect != null)
            {
                effect.SetupCamera(camera);
            }

#if UNITY_EDITOR

            if (Application.isPlaying)
            {

#endif

                UpdateLensFlare(camera);
                CloudShadowProjectorScript.RenderCloudShadows(camera);

#if UNITY_EDITOR

            }

#endif

        }

        /// <summary>
        /// Show clouds animated. Animates cover, density, sharpness, light absorption and color
        /// </summary>
        /// <param name="duration">Transition duration in seconds</param>
        /// <param name="profileName">Cloud profile name</param>
        public void ShowCloudsAnimated(float duration, string profileName)
        {
            ShowCloudsAnimated(duration, Resources.Load<WeatherMakerCloudProfileScript>(profileName));
        }

        /// <summary>
        /// Show clouds animated. Animates cover, density, sharpness, light absorption and color
        /// </summary>
        /// <param name="duration">Transition duration in seconds</param>
        /// <param name="profile">Cloud profile, or pass null to hide clouds</param>
        public void ShowCloudsAnimated(float duration, WeatherMakerCloudProfileScript profile)
        {
            WeatherMakerCloudProfileScript oldProfile = CloudProfile;

            // dynamic start and end properties
            float endCover, endCover2, endCover3, endCover4;
            float endDensity, endDensity2, endDensity3, endDensity4;
            float startSharpness, startSharpness2, startSharpness3, startSharpness4;
            float endSharpness, endSharpness2, endSharpness3, endSharpness4;
            Vector4 startScale, startScale2, startScale3, startScale4;
            Vector4 endScale, endScale2, endScale3, endScale4;
            float startMaskScale, startMaskScale2, startMaskScale3, startMaskScale4;
            float endMaskScale, endMaskScale2, endMaskScale3, endMaskScale4;
            float startHeight, startHeight2, startHeight3, startHeight4;
            float endHeight, endHeight2, endHeight3, endHeight4;

            if (profile == null)
            {
                // copy profile and set properties to clear the clouds
                _CloudProfile = oldProfile;
                CopyCurrentProfile();

                // zero out the necessary properties to clear all clouds
                endCover = endCover2 = endCover3 = endCover4 = 0.0f;
                endDensity = endDensity2 = endDensity3 = endDensity4 = 0.0f;
            }
            else
            {
                // transition from one set of clouds to another
                _CloudProfile = profile;
                CopyCurrentProfile();

                // apply end cover and density
                endCover = CloudProfile.CloudCover;
                endCover2 = CloudProfile.CloudCover2;
                endCover3 = CloudProfile.CloudCover3;
                endCover4 = CloudProfile.CloudCover4;
                endDensity = CloudProfile.CloudDensity;
                endDensity2 = CloudProfile.CloudDensity2;
                endDensity3 = CloudProfile.CloudDensity3;
                endDensity4 = CloudProfile.CloudDensity4;
            }

            float startCover = oldProfile.CloudCover;
            float startCover2 = oldProfile.CloudCover2;
            float startCover3 = oldProfile.CloudCover3;
            float startCover4 = oldProfile.CloudCover4;
            float startDensity = oldProfile.CloudDensity;
            float startDensity2 = oldProfile.CloudDensity2;
            float startDensity3 = oldProfile.CloudDensity3;
            float startDensity4 = oldProfile.CloudDensity4;
            float startLightAbsorption = oldProfile.CloudLightAbsorption;
            float startLightAbsorption2 = oldProfile.CloudLightAbsorption2;
            float startLightAbsorption3 = oldProfile.CloudLightAbsorption3;
            float startLightAbsorption4 = oldProfile.CloudLightAbsorption4;
            Color startColor = oldProfile.CloudColor;
            Color startColor2 = oldProfile.CloudColor2;
            Color startColor3 = oldProfile.CloudColor3;
            Color startColor4 = oldProfile.CloudColor4;
            Vector4 startMultiplier = oldProfile.CloudNoiseMultiplier;
            Vector4 startMultiplier2 = oldProfile.CloudNoiseMultiplier2;
            Vector4 startMultiplier3 = oldProfile.CloudNoiseMultiplier3;
            Vector4 startMultiplier4 = oldProfile.CloudNoiseMultiplier4;
            Vector4 startVelocity = oldProfile.CloudNoiseVelocity;
            Vector4 startVelocity2 = oldProfile.CloudNoiseVelocity2;
            Vector4 startVelocity3 = oldProfile.CloudNoiseVelocity3;
            Vector4 startVelocity4 = oldProfile.CloudNoiseVelocity4;
            Vector4 startMaskVelocity = oldProfile.CloudNoiseMaskVelocity;
            Vector4 startMaskVelocity2 = oldProfile.CloudNoiseMaskVelocity2;
            Vector4 startMaskVelocity3 = oldProfile.CloudNoiseMaskVelocity3;
            Vector4 startMaskVelocity4 = oldProfile.CloudNoiseMaskVelocity4;
            Vector4 startRotation = new Vector4(oldProfile.CloudNoiseRotation.LastValue, oldProfile.CloudNoiseRotation2.LastValue, oldProfile.CloudNoiseRotation3.LastValue, oldProfile.CloudNoiseRotation4.LastValue);
            Vector4 startMaskRotation = new Vector4(oldProfile.CloudNoiseMaskRotation.LastValue, oldProfile.CloudNoiseMaskRotation2.LastValue, oldProfile.CloudNoiseMaskRotation3.LastValue, oldProfile.CloudNoiseMaskRotation4.LastValue);

            float endLightAbsorption = CloudProfile.CloudLightAbsorption;
            float endLightAbsorption2 = CloudProfile.CloudLightAbsorption2;
            float endLightAbsorption3 = CloudProfile.CloudLightAbsorption3;
            float endLightAbsorption4 = CloudProfile.CloudLightAbsorption4;
            Color endColor = CloudProfile.CloudColor;
            Color endColor2 = CloudProfile.CloudColor2;
            Color endColor3 = CloudProfile.CloudColor3;
            Color endColor4 = CloudProfile.CloudColor4;
            Vector4 endMultiplier = CloudProfile.CloudNoiseMultiplier;
            Vector4 endMultiplier2 = CloudProfile.CloudNoiseMultiplier2;
            Vector4 endMultiplier3 = CloudProfile.CloudNoiseMultiplier3;
            Vector4 endMultiplier4 = CloudProfile.CloudNoiseMultiplier4;
            Vector3 endVelocity = CloudProfile.CloudNoiseVelocity;
            Vector3 endVelocity2 = CloudProfile.CloudNoiseVelocity2;
            Vector3 endVelocity3 = CloudProfile.CloudNoiseVelocity3;
            Vector3 endVelocity4 = CloudProfile.CloudNoiseVelocity4;
            Vector3 endMaskVelocity = CloudProfile.CloudNoiseMaskVelocity;
            Vector3 endMaskVelocity2 = CloudProfile.CloudNoiseMaskVelocity2;
            Vector3 endMaskVelocity3 = CloudProfile.CloudNoiseMaskVelocity3;
            Vector3 endMaskVelocity4 = CloudProfile.CloudNoiseMaskVelocity4;
            Vector4 endRotation = new Vector4(CloudProfile.CloudNoiseRotation.Random(), CloudProfile.CloudNoiseRotation2.Random(), CloudProfile.CloudNoiseRotation3.Random(), CloudProfile.CloudNoiseRotation4.Random());
            Vector4 endMaskRotation = new Vector4(CloudProfile.CloudNoiseMaskRotation.Random(), CloudProfile.CloudNoiseMaskRotation2.Random(), CloudProfile.CloudNoiseMaskRotation3.Random(), CloudProfile.CloudNoiseMaskRotation4.Random());

            if (startCover == 0.0f)
            {
                // transition from no clouds, jump to new profile
                startSharpness = endSharpness = _CloudProfile.CloudSharpness;
                startSharpness2 = endSharpness2 = _CloudProfile.CloudSharpness2;
                startSharpness3 = endSharpness3 = _CloudProfile.CloudSharpness3;
                startSharpness4 = endSharpness4 = _CloudProfile.CloudSharpness4;
                startScale = endScale = _CloudProfile.CloudNoiseScale;
                startScale2 = endScale2 = _CloudProfile.CloudNoiseScale2;
                startScale3 = endScale3 = _CloudProfile.CloudNoiseScale3;
                startScale4 = endScale4 = _CloudProfile.CloudNoiseScale4;
                startMaskScale = endMaskScale = _CloudProfile.CloudNoiseMaskScale;
                startMaskScale2 = endMaskScale2 = _CloudProfile.CloudNoiseMaskScale2;
                startMaskScale3 = endMaskScale3 = _CloudProfile.CloudNoiseMaskScale3;
                startMaskScale4 = endMaskScale4 = _CloudProfile.CloudNoiseMaskScale4;
                startHeight = endHeight = _CloudProfile.CloudHeight;
                startHeight2 = endHeight2 = _CloudProfile.CloudHeight2;
                startHeight3 = endHeight3 = _CloudProfile.CloudHeight3;
                startHeight4 = endHeight4 = _CloudProfile.CloudHeight4;
            }
            else if (endCover == 0.0f)
            {
                // transition from clouds to no clouds, jump to old profile
                startSharpness = endSharpness = oldProfile.CloudSharpness;
                startSharpness2 = endSharpness2 = oldProfile.CloudSharpness2;
                startSharpness3 = endSharpness3 = oldProfile.CloudSharpness3;
                startSharpness4 = endSharpness4 = oldProfile.CloudSharpness4;
                startScale = endScale = oldProfile.CloudNoiseScale;
                startScale2 = endScale2 = oldProfile.CloudNoiseScale2;
                startScale3 = endScale3 = oldProfile.CloudNoiseScale3;
                startScale4 = endScale4 = oldProfile.CloudNoiseScale4;
                startMaskScale = endMaskScale = oldProfile.CloudNoiseMaskScale;
                startMaskScale2 = endMaskScale2 = oldProfile.CloudNoiseMaskScale2;
                startMaskScale3 = endMaskScale3 = oldProfile.CloudNoiseMaskScale3;
                startMaskScale4 = endMaskScale4 = oldProfile.CloudNoiseMaskScale4;
                startHeight = endHeight = oldProfile.CloudHeight;
                startHeight2 = endHeight2 = oldProfile.CloudHeight2;
                startHeight3 = endHeight3 = oldProfile.CloudHeight3;
                startHeight4 = endHeight4 = oldProfile.CloudHeight4;
            }
            else
            {
                // regular transition from one clouds to another, transition normally
                startScale = oldProfile.CloudNoiseScale;
                startScale2 = oldProfile.CloudNoiseScale2;
                startScale3 = oldProfile.CloudNoiseScale3;
                startScale4 = oldProfile.CloudNoiseScale4;
                endScale = CloudProfile.CloudNoiseScale;
                endScale2 = CloudProfile.CloudNoiseScale2;
                endScale3 = CloudProfile.CloudNoiseScale3;
                endScale4 = CloudProfile.CloudNoiseScale4;
                startMaskScale = oldProfile.CloudNoiseMaskScale;
                startMaskScale2 = oldProfile.CloudNoiseMaskScale2;
                startMaskScale3 = oldProfile.CloudNoiseMaskScale3;
                startMaskScale4 = oldProfile.CloudNoiseMaskScale4;
                endMaskScale = CloudProfile.CloudNoiseMaskScale;
                endMaskScale2 = CloudProfile.CloudNoiseMaskScale2;
                endMaskScale3 = CloudProfile.CloudNoiseMaskScale3;
                endMaskScale4 = CloudProfile.CloudNoiseMaskScale4;
                startSharpness = oldProfile.CloudSharpness;
                startSharpness2 = oldProfile.CloudSharpness2;
                startSharpness3 = oldProfile.CloudSharpness3;
                startSharpness4 = oldProfile.CloudSharpness4;
                endSharpness = _CloudProfile.CloudSharpness;
                endSharpness2 = _CloudProfile.CloudSharpness2;
                endSharpness3 = _CloudProfile.CloudSharpness3;
                endSharpness4 = _CloudProfile.CloudSharpness4;
                startHeight = oldProfile.CloudHeight;
                startHeight2 = oldProfile.CloudHeight2;
                startHeight3 = oldProfile.CloudHeight3;
                startHeight4 = oldProfile.CloudHeight4;
                endHeight = CloudProfile.CloudHeight;
                endHeight2 = CloudProfile.CloudHeight2;
                endHeight3 = CloudProfile.CloudHeight3;
                endHeight4 = CloudProfile.CloudHeight4;
            }

            oldProfile.CopyStateTo(_CloudProfile);

            TweenFactory.Tween("WeatherMakerClouds", 0.0f, 1.0f, duration, TweenScaleFunctions.Linear, (ITween<float> c) =>
            {
                float progress = c.CurrentValue;
                CloudProfile.CloudNoiseScale = Vector4.Lerp(startScale, endScale, progress);
                CloudProfile.CloudNoiseScale2 = Vector4.Lerp(startScale2, endScale2, progress);
                CloudProfile.CloudNoiseScale3 = Vector4.Lerp(startScale3, endScale3, progress);
                CloudProfile.CloudNoiseScale4 = Vector4.Lerp(startScale4, endScale4, progress);
                CloudProfile.CloudNoiseMultiplier = Vector4.Lerp(startMultiplier, endMultiplier, progress);
                CloudProfile.CloudNoiseMultiplier2 = Vector4.Lerp(startMultiplier2, endMultiplier2, progress);
                CloudProfile.CloudNoiseMultiplier3 = Vector4.Lerp(startMultiplier3, endMultiplier3, progress);
                CloudProfile.CloudNoiseMultiplier4 = Vector4.Lerp(startMultiplier4, endMultiplier4, progress);
                CloudProfile.CloudNoiseRotation.LastValue = Mathf.Lerp(startRotation.x, endRotation.x, progress);
                CloudProfile.CloudNoiseRotation2.LastValue = Mathf.Lerp(startRotation.y, endRotation.y, progress);
                CloudProfile.CloudNoiseRotation3.LastValue = Mathf.Lerp(startRotation.z, endRotation.z, progress);
                CloudProfile.CloudNoiseRotation4.LastValue = Mathf.Lerp(startRotation.w, endRotation.w, progress);
                CloudProfile.CloudNoiseVelocity = Vector3.Lerp(startVelocity, endVelocity, progress);
                CloudProfile.CloudNoiseVelocity2 = Vector3.Lerp(startVelocity2, endVelocity2, progress);
                CloudProfile.CloudNoiseVelocity3 = Vector3.Lerp(startVelocity3, endVelocity3, progress);
                CloudProfile.CloudNoiseVelocity4 = Vector3.Lerp(startVelocity4, endVelocity4, progress);
                CloudProfile.CloudNoiseMaskScale = Mathf.Lerp(startMaskScale, endMaskScale, progress);
                CloudProfile.CloudNoiseMaskScale2 = Mathf.Lerp(startMaskScale2, endMaskScale2, progress);
                CloudProfile.CloudNoiseMaskScale3 = Mathf.Lerp(startMaskScale3, endMaskScale3, progress);
                CloudProfile.CloudNoiseMaskScale4 = Mathf.Lerp(startMaskScale4, endMaskScale4, progress);
                CloudProfile.CloudNoiseMaskRotation.LastValue = Mathf.Lerp(startMaskRotation.x, endMaskRotation.x, progress);
                CloudProfile.CloudNoiseMaskRotation2.LastValue = Mathf.Lerp(startMaskRotation.y, endMaskRotation.y, progress);
                CloudProfile.CloudNoiseMaskRotation3.LastValue = Mathf.Lerp(startMaskRotation.z, endMaskRotation.z, progress);
                CloudProfile.CloudNoiseMaskRotation4.LastValue = Mathf.Lerp(startMaskRotation.w, endMaskRotation.w, progress);
                CloudProfile.CloudNoiseMaskVelocity = Vector3.Lerp(startMaskVelocity, endMaskVelocity, progress);
                CloudProfile.CloudNoiseMaskVelocity2 = Vector3.Lerp(startMaskVelocity2, endMaskVelocity2, progress);
                CloudProfile.CloudNoiseMaskVelocity3 = Vector3.Lerp(startMaskVelocity3, endMaskVelocity3, progress);
                CloudProfile.CloudNoiseMaskVelocity4 = Vector3.Lerp(startMaskVelocity4, endMaskVelocity4, progress);
                CloudProfile.CloudCover = Mathf.Lerp(startCover, endCover, progress);
                CloudProfile.CloudCover2 = Mathf.Lerp(startCover2, endCover2, progress);
                CloudProfile.CloudCover3 = Mathf.Lerp(startCover3, endCover3, progress);
                CloudProfile.CloudCover4 = Mathf.Lerp(startCover4, endCover4, progress);
                CloudProfile.CloudDensity = Mathf.Lerp(startDensity, endDensity, progress);
                CloudProfile.CloudDensity2 = Mathf.Lerp(startDensity2, endDensity2, progress);
                CloudProfile.CloudDensity3 = Mathf.Lerp(startDensity3, endDensity3, progress);
                CloudProfile.CloudDensity4 = Mathf.Lerp(startDensity4, endDensity4, progress);
                CloudProfile.CloudSharpness = Mathf.Lerp(startSharpness, endSharpness, progress);
                CloudProfile.CloudSharpness2 = Mathf.Lerp(startSharpness2, endSharpness2, progress);
                CloudProfile.CloudSharpness3 = Mathf.Lerp(startSharpness3, endSharpness3, progress);
                CloudProfile.CloudSharpness4 = Mathf.Lerp(startSharpness4, endSharpness4, progress);
                CloudProfile.CloudLightAbsorption = Mathf.Lerp(startLightAbsorption, endLightAbsorption, progress);
                CloudProfile.CloudLightAbsorption2 = Mathf.Lerp(startLightAbsorption2, endLightAbsorption2, progress);
                CloudProfile.CloudLightAbsorption3 = Mathf.Lerp(startLightAbsorption3, endLightAbsorption3, progress);
                CloudProfile.CloudLightAbsorption4 = Mathf.Lerp(startLightAbsorption4, endLightAbsorption4, progress);
                CloudProfile.CloudColor = Color.Lerp(startColor, endColor, progress);
                CloudProfile.CloudColor2 = Color.Lerp(startColor2, endColor2, progress);
                CloudProfile.CloudColor3 = Color.Lerp(startColor3, endColor3, progress);
                CloudProfile.CloudColor4 = Color.Lerp(startColor4, endColor4, progress);
                CloudProfile.CloudHeight = Mathf.Lerp(startHeight, endHeight, progress);
                CloudProfile.CloudHeight2 = Mathf.Lerp(startHeight2, endHeight2, progress);
                CloudProfile.CloudHeight3 = Mathf.Lerp(startHeight3, endHeight3, progress);
                CloudProfile.CloudHeight4 = Mathf.Lerp(startHeight4, endHeight4, progress);
            }, (ITween<float> c) =>
            {
                if (oldProfile.name.Contains("(Clone)"))
                {
                    GameObject.Destroy(oldProfile);
                }
            });
        }

        /// <summary>
        /// Hide clouds animated, all layers
        /// </summary>
        /// <param name="duration">Transition duration in seconds</param>
        public void HideCloudsAnimated(float duration)
        {
            ShowCloudsAnimated(duration, (WeatherMakerCloudProfileScript)null);
        }
    }
}

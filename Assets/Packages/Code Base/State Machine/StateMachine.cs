﻿using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Zac
{
	public class StateMachine : MonoBehaviour
	{
		#region Enumerations
		private enum StateMode
		{
			Enter,
			Update,
			Exit,
			Condition,
		}
		#endregion

		#region Event Handlers
		public delegate void OnStateEventHandler();
		public delegate void OnStateEventAttributeHandler(object state);

		private event OnStateEventHandler OnStateEnterInternal;
		private event OnStateEventHandler OnStateUpdateInternal;
		private event OnStateEventHandler OnStateExitInternal;

		protected event OnStateEventHandler OnStateEnter;
		protected event OnStateEventHandler OnStateUpdate;
		protected event OnStateEventHandler OnStateExit;
		#endregion

		#region Private Variables
		private BindingFlags BINDING_FLAGS = BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance;

		private IDictionary m_states;
		private Type m_type;
		
		private object m_currentState;

		private object m_instance;
		private Type m_instanceType;

		private Queue<TransitionRequest> m_transitionRequests;
		private TransitionRequest m_currentTransitionRequest;
		#endregion

		#region Properties
		private bool Initialized
		{
			get;
			set;
		}
		
		private object CurrentState
		{
			get
			{
				return m_currentState;
			}
			set
			{
				// TODO: Validate to ensure the enum is being used. (or string version exists within the enumLookup)
				m_currentState = value.ToString();
			}
		}

		protected object PreviousState
		{
			get;
			private set;
		}
		#endregion

		#region UnityEvents
		protected virtual void Awake()
		{
			SubscribeEvents();
		}

		protected virtual void OnDestroy()
		{
			UnsubscribeEvents();
		}
		#endregion

		#region Protected Functionality
		protected void Initialize<T>(T initialState)
		{
			if (Validate<T>())
			{
				m_instanceType = GetType().UnderlyingSystemType;
				m_instance = Convert.ChangeType(this, m_instanceType);

				m_type = typeof(T);
				m_states = new Dictionary<string, StateInfo>();
				m_transitionRequests = new Queue<TransitionRequest>();

				int enumLength = Enum.GetValues(typeof(T)).Length;

				List<string> stateNames = new List<string>();

				// Iterate through each state in the provided enumeration.
				for (int i = 0; i < enumLength; ++i)
				{
					string state = ((T)(object)i).ToString();
					if (!m_states.Contains(state))
					{
						m_states.Add(state.ToString(), new StateInfo(m_instance));
						stateNames.Add(state.ToString());
					}

					// Scan the derived class for events for this state
					foreach (StateMode stateMode in Enum.GetValues(typeof(StateMode)))
					{
						MethodInfo method = GetMethod(state, stateMode);

						if (method != null)
						{
							((Dictionary<string, StateInfo>)m_states)[state].AddEvent(stateMode, (StateMethodInfo)method);
						}
					}
				}
				AcquireTransitions(stateNames);
				ChangeState(initialState);

				Initialized = true;
			}
		}

		protected void Transition<T>(T state)
		{
			m_transitionRequests.Enqueue(new TransitionRequest(gameObject, 0, () =>
			{
				ChangeState(state);
				m_currentTransitionRequest = null;
			}));
		}

		protected T GetCurrentState<T>()
		{
			if(Validate<T>())
			{
				return (T)Enum.Parse(typeof(T), CurrentState.ToString());
			}
			else
			{
#if UNITY_EDITOR
				Debug.Log("An invalid type was passed to the GetCurrentState function.");
#endif
			}
			return (T)(object)null;
		}
#endregion

#region Private Functionality
		private void AcquireTransitions(List<string> stateNames)
		{
			MethodInfo[] methodList = GetType().GetMethods(BINDING_FLAGS);

			for(int i = 0; i < m_states.Count; ++i)
			{
				for (int j = 0; j < m_states.Count; ++j)
				{
					if (i != j) // Prevent conditions from/to the same state
					{
						string methodPrefix = stateNames[i] + "_" + stateNames[j] + "_" + "Condition";
						
						// TODO - Optimisation: Iterate backwards, and remove any that do not contain "Condition" from future checks.
						foreach (MethodInfo method in methodList)
						{
							if (method.Name == methodPrefix || method.Name.StartsWith(methodPrefix + "_"))
							{
								StateMethodInfoAttributes stateMethod = new StateMethodInfoAttributes(method, stateNames[j]);
								
								((Dictionary<string, StateInfo>)m_states)[stateNames[i]].AddEvent(StateMode.Condition, stateMethod);
							}
						}
					}
				}
			}
		}

		private void ChangeState(object state)
		{
			PreviousState = CurrentState;
			CurrentState = state;

			RaiseEvent(OnStateEnter);
			RaiseEvent(OnStateEnterInternal);
		}

		private MethodInfo GetMethod(object state, StateMode stateMode)
		{
			return GetType().GetMethod(state.ToString() + "_" + stateMode.ToString(), BINDING_FLAGS);
		}

		private MethodInfo GetMethod(object state, object state2, StateMode stateMode)
		{
			return GetType().GetMethod(state.ToString() + "_" + state2.ToString() + "_" + stateMode.ToString(), BINDING_FLAGS);
		}

		private bool Validate<T>()
		{
			return typeof(T).BaseType == typeof(Enum) && (m_type != null ? typeof(T) == m_type : true);
		}

		protected virtual void Update()
		{
			if (Initialized)
			{
				if (m_transitionRequests.Count > 0 && m_currentTransitionRequest == null)
				{
					RaiseEvent(OnStateExit);
					RaiseEvent(OnStateExitInternal);

					m_currentTransitionRequest = m_transitionRequests.Dequeue();
					m_currentTransitionRequest.Transition();
				}
				else
				{
					if (m_currentTransitionRequest == null)
					{
						RaiseEvent(OnStateUpdate);
						RaiseEvent(OnStateUpdateInternal);
					}
				}
			}
		}

		private void Execute(StateMode stateMode)
		{
			if (m_currentState != null)
			{
				if (stateMode == StateMode.Condition)
				{
					((StateInfo)m_states[m_currentState]).InvokeConditions((state) =>
					{
						Transition(state);
					});
				}
				else
				{
					((StateInfo)m_states[m_currentState]).InvokeEvents(stateMode);
				}
			}
		}
#endregion

#region Event Management
		private void SubscribeEvents()
		{
			OnStateEnterInternal += OnStateEnterCallback;
			OnStateUpdateInternal += OnStateUpdateCallback;
			OnStateExitInternal += OnStateExitCallback;
		}

		private void UnsubscribeEvents()
		{
			OnStateEnterInternal -= OnStateEnterCallback;
			OnStateUpdateInternal -= OnStateUpdateCallback;
			OnStateExitInternal -= OnStateExitCallback;
		}

		private void RaiseEvent(OnStateEventHandler stateEvent)
		{
			if (stateEvent != null)
			{
				stateEvent.Invoke();
			}
		}

#region Events
		private void OnStateEnterCallback()
		{
			Execute(StateMode.Enter);
		}

		private void OnStateUpdateCallback()
		{
			Execute(StateMode.Update);
			Execute(StateMode.Condition);
		}

		private void OnStateExitCallback()
		{
			Execute(StateMode.Exit);
		}
#endregion
#endregion

#region Supporting Classes
		class TransitionRequest
		{
			// TODO: Add a transition callback so processing can occurr during the transition.
			// TODO: Store the from and to, and prevent identical consecutive transitions being queued.
			public delegate void TransitionHandler();

			private GameObject			m_parent;
			private TransitionHandler	m_transitionCallback;
			private float				m_duration;

			public TransitionRequest(GameObject parent, float duration, TransitionHandler callback)
			{
				m_parent = parent;
				m_transitionCallback = callback;
				m_duration = duration;
			}

			public void Transition()
			{
				m_parent.GetComponent<StateMachine>().StartCoroutine(InternalTransition());
			}

			private IEnumerator InternalTransition()
			{
				yield return new WaitForSeconds(m_duration);

				m_transitionCallback.Invoke();
			}
		}

		class StateInfo
		{
			private object					m_instance;
			private List<List<StateMethodInfo>>	m_stateEvents;

			public StateInfo(object instance)
			{
				m_instance = instance;
				m_stateEvents = new List<List<StateMethodInfo>>();

				foreach (StateMode stateMode in Enum.GetValues(typeof(StateMode)))
				{
					m_stateEvents.Add(new List<StateMethodInfo>());
				}
			}

			public void AddEvent(StateMode stateMode, StateMethodInfo methodInfo)
			{
				if (ValidateMethod(stateMode, (MethodInfo)methodInfo))
				{
					m_stateEvents[(int)stateMode].Add(methodInfo);
				}
#if UNITY_EDITOR
				else
				{
					Debug.LogError("An invalid method assignment was requested\n" + methodInfo.MethodInfo.Name);
				}
#endif
			}
		
			public void InvokeEvents(StateMode stateMode)
			{
				for(int i = 0; i < m_stateEvents[(int)stateMode].Count; ++i)
				{
					if(m_stateEvents[(int)stateMode][i] != null)
					{
						m_stateEvents[(int)stateMode][i].MethodInfo.Invoke(m_instance, null);
					}
				}
			}

			public void InvokeConditions(OnStateEventAttributeHandler callback)
			{
				StateMethodInfo currentEvent;

				for(int i = 0; i < m_stateEvents[(int)StateMode.Condition].Count; ++i)
				{
					currentEvent = m_stateEvents[(int)StateMode.Condition][i];
					if((bool)currentEvent.MethodInfo.Invoke(m_instance, null))
					{
						callback(((StateMethodInfoAttributes)currentEvent).State);
						break;
					}
				}
			}

			public List<StateMethodInfo> GetEvents(StateMode stateMode)
			{
				return m_stateEvents[(int)stateMode];
			}

			private bool ValidateMethod(StateMode stateMode, MethodInfo methodInfo)
			{
				bool validation = true;

				switch (stateMode)
				{
					case StateMode.Enter:
					case StateMode.Update:
					case StateMode.Exit:
						validation &= methodInfo.ReturnType == typeof(void) && methodInfo.GetParameters().Length == 0;
						break;
					case StateMode.Condition:
						validation &= methodInfo.ReturnType == typeof(bool) && methodInfo.GetParameters().Length == 0;
						break;
					default:
						break;
				}
				return validation;
			}
		}

		class StateMethodInfo
		{
			public StateMethodInfo(MethodInfo methodInfo)
			{
				MethodInfo = methodInfo;
			}

			public static explicit operator MethodInfo(StateMethodInfo stateMethodInfo)
			{
				return stateMethodInfo.MethodInfo;
			}

			public static explicit operator StateMethodInfo(MethodInfo methodInfo)
			{
				StateMethodInfo stateMethodInfo = new StateMethodInfo(methodInfo);
				return stateMethodInfo;
			}

			public MethodInfo MethodInfo
			{
				get;
				private set;
			}
		}

		class StateMethodInfoAttributes : StateMethodInfo
		{
			public StateMethodInfoAttributes(MethodInfo methodInfo, object state) : base(methodInfo)
			{
				State = state;
			}

			public object State
			{
				get;
				private set;
			}
		}
#endregion
	}
}
﻿namespace Zac
{
	public interface IEventListener
	{
		void HandleEvent(EventHandler.EventType eventType, params object[] objects);
	}
}
﻿using System;
using System.Collections.Generic;

namespace Zac
{
	public class EventHandler : Singleton<EventHandler>
	{
		[Flags]
		public enum EventType
		{

		}

		[Flags]
		public enum EventLayer
		{
			All = 0,
		}

		private Dictionary<EventType, List<IEventListener>>		m_eventListeners;
		private Dictionary<IEventListener, List<EventLayer>>	m_eventListenerLayers;

		private void Awake()
		{
			m_eventListeners		= new Dictionary<EventType, List<IEventListener>>();
			m_eventListenerLayers	= new Dictionary<IEventListener, List<EventLayer>>();

			foreach (EventType eventType in Enum.GetValues(typeof(EventType)))
			{
				m_eventListeners.Add(eventType, new List<IEventListener>());
			}
		}

		public void RegisterListener<T>(T instance, EventType eventTypes) where T : IEventListener
		{
			foreach (EventType eventType in Enum.GetValues(typeof(EventType)))
			{
				if ((eventTypes & eventType) == eventType)
				{
					if (!m_eventListeners[eventType].Contains(instance))
					{
						m_eventListeners[eventType].Add(instance);
					}
				}
			}
		}

		public void DeregisterListener<T>(T instance, EventType eventTypes) where T : IEventListener
		{
			foreach (EventType eventType in Enum.GetValues(typeof(EventType)))
			{
				if ((eventTypes & eventType) == eventType)
				{
					m_eventListeners[eventType].Remove(instance);
				}
			}
		}

		public void RegisterLayers<T>(T instance, EventLayer eventLayers) where T : IEventListener
		{
			foreach (EventLayer eventLayer in Enum.GetValues(typeof(EventLayer)))
			{
				if ((eventLayers & eventLayer) == eventLayer)
				{
					if (!m_eventListenerLayers.ContainsKey(instance))
					{
						m_eventListenerLayers.Add(instance, new List<EventLayer>() { eventLayer });
					}
					else if (!m_eventListenerLayers[instance].Contains(eventLayer))
					{
						m_eventListenerLayers[instance].Add(eventLayer);
					}
				}
			}
		}

		public void DeregisterLayers<T>(T instance, EventLayer eventLayers) where T : IEventListener
		{
			foreach (EventLayer eventLayer in Enum.GetValues(typeof(EventLayer)))
			{
				if((eventLayers & eventLayer) == eventLayer)
				{
					if(m_eventListenerLayers.ContainsKey(instance))
					{
						m_eventListenerLayers[instance].Remove(eventLayer);
					}
					else
					{
						break;
					}
				}
			}
		}
		
		public void FireEvent(EventType eventType, EventLayer eventLayers, params object[] objects)
		{
			if (eventLayers == 0)
			{
				foreach (IEventListener listener in m_eventListeners[eventType])
				{
					listener.HandleEvent(eventType, objects);
				}
			}
			else
			{
				foreach (IEventListener listener in m_eventListeners[eventType])
				{
					if (m_eventListenerLayers.ContainsKey(listener))
					{
						foreach (EventLayer eventLayer in Enum.GetValues(typeof(EventLayer)))
						{
							if ((eventLayer & eventLayers) == eventLayer)
							{
								if (m_eventListenerLayers[listener].Contains(eventLayer))
								{
									listener.HandleEvent(eventType, objects);
									break;
								}
							}
						}
					}
					else
					{
						listener.HandleEvent(eventType, objects);
					}
				}
			}
		}
	}
}
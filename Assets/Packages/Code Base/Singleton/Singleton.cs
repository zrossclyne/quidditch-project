﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Zac
{
	public class Singleton<T> : MonoBehaviour where T : Component
	{
		[SerializeField] private bool m_persistant;

		private static T m_instance;

		public static T Instance
		{
			get
			{
				if (m_instance == null)
				{
					GameObject instance = new GameObject("[" + typeof(T).ToString() + "]");
					m_instance = instance.AddComponent<T>();
				}

				return m_instance;
			}
		}

		private void Awake()
		{
			SceneManager.sceneLoaded += OnSceneLoaded;

			if (m_instance != null)
			{
				Destroy(gameObject);
			}
			else
			{
				m_instance = this as T;

				if (m_persistant)
				{
					DontDestroyOnLoad(gameObject);
				}
			}
		}

		private void OnDestroy()
		{
			SceneManager.sceneLoaded -= OnSceneLoaded;
		}

		private void OnSceneLoaded(Scene level, LoadSceneMode loadMode)
		{
			if (!m_persistant)
			{
				Destroy(gameObject);
			}
		}
	}
}